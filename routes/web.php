<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['middleware' => ['can:administer']], function () {
        Route::get('/users', 'UserController@index')
            ->name('users');
        Route::post('/users', 'UserController@store');
        Route::patch('/users/{user}', 'UserController@update')
            ->name('users.self');
        Route::delete('/users/{user}', 'UserController@remove');

        Route::get('/clients', 'ClientController@index')
            ->name('clients');
        Route::post('/clients', 'ClientController@store');
        Route::get('/clients/create', 'ClientController@create')
            ->name('clients.create');
        Route::put('/clients/{client}/active', 'ClientController@activate')
            ->name('clients.active');
        Route::delete('/clients/{client}/active', 'ClientController@deactivate');
        Route::patch('/clients/{client}', 'ClientController@update')
            ->name('clients.self');
        Route::delete('/clients/{client}', 'ClientController@remove');

        Route::get('/{client}/config', 'MonitoringConfigController@index')
            ->name('clients.config');
        Route::post('/{client}/config', 'MonitoringConfigController@store');
        Route::delete('/{client}/config/{config}', 'MonitoringConfigController@remove')
            ->name('clients.config.delete');
    });

    Route::group(['middleware' => ['can:monitoring,client']], function () {
        Route::post('/{client}/snippets/{snippet}', 'SnippetController@update')
            ->name('snippets.update');
        Route::post('/{client}/snippets', 'SnippetController@bulkUpdate')
            ->name('snippets.bulk_update');
        Route::get('/{client}/categorized', 'MonitoringController@categorized')
            ->name('monitoring.categorized');
        Route::get('/{client}/uncategorized', 'MonitoringController@uncategorized')
            ->name('monitoring.uncategorized');
        Route::get('/{client}/search', 'MonitoringController@search')
            ->name('monitoring.search');
        Route::get('/{client}/report/daily', 'ReportController@daily')
            ->name('reports.daily');
        Route::get('/{client}/report/monthly', 'ReportController@monthly')
            ->name('reports.monthly');
        Route::get('/{client}', 'MonitoringController@index')
            ->name('monitoring');
    });
});
