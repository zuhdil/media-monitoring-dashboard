var FormModel = require('./form-model');
var notification = require('./notification');

function EditModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));

    container.on('hidden.bs.modal', function () {
        form.reset();
        form.data('client', null);
    });

    container.on('shown.bs.modal', function () {
        form.focus('name');
    });

    form.submit(function (e) {
        var client = form.data('client');

        $.ajax({
            type: 'patch',
            url: client.links._self,
            data: form.serialize(),
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
            container.modal('hide');
        }).fail(function (xhr) {
            if (xhr.status == 422) {
                var errors = xhr.responseJSON;
                form.errors(errors);
            } else {
                handler.failure(xhr.statusText);
                container.modal('hide');
            }
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (client) {
            form.data('client', client);
            form.input('name', client.name);
            form.input('username', client.username);
            container.modal('show');
        }
    };
}

function ResetPasswordModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));

    container.on('hidden.bs.modal', function () {
        form.reset();
        form.data('client', null);
    });

    container.on('shown.bs.modal', function () {
        form.focus('password');
    });

    form.submit(function (e) {
        var client = form.data('client');

        $.ajax({
            type: 'patch',
            url: client.links._self,
            data: form.serialize(),
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
            container.modal('hide');
        }).fail(function (xhr) {
            if (xhr.status == 422) {
                var errors = xhr.responseJSON;
                form.errors(errors);
            } else {
                handler.failure(xhr.statusText);
                container.modal('hide');
            }
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (client) {
            form.data('client', client);
            form.focus('password');
            container.modal('show');
        }
    };
}

function ActivationModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));
    var title = container.find('.modal-title');
    var confirmation = container.find('.confirmation');

    container.on('hidden.bs.modal', function () {
        title.html('');
        confirmation.html('');
    });

    form.submit(function (e) {
        var client = form.data('client');

        $.ajax({
            type: client.is_active ? 'delete' : 'put',
            url: client.links.active,
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
        }).fail(function (xhr) {
            handler.failure(xhr.statusText);
        }).always(function () {
            container.modal('hide');
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (client) {
            var action = client.is_active ? 'Deactivate' : 'Activate';
            form.data('client', client);
            title.html( action + ' client');
            confirmation.html(action + ' <strong>'+client.name+'</strong>?');
            container.modal('show');
        }
    };
}

function DeleteModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));
    var confirmation = container.find('.confirmation');

    container.on('hidden.bs.modal', function () {
        confirmation.html('');
    });

    form.submit(function (e) {
        var client = form.data('client');

        $.ajax({
            type: 'delete',
            url: client.links._self,
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
        }).fail(function (xhr) {
            handler.failure(xhr.statusText);
        }).always(function () {
            container.modal('hide');
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (client) {
            form.data('client', client);
            confirmation.html('Delete <strong>'+client.name+'</strong>?');
            container.modal('show');
        }
    };
}

module.exports = function (selector, dataurl, modals) {
    var columns = {'0':'name', '1':'username', '3':'created_at'};
    var container = $(selector);

    var table = container.DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        columns: [
            {
                data: 'name'
            },
            {
                data: 'username'
            },
            {
                data: 'keywords',
                orderable: false
            },
            {
                data: 'created_at',
                className: 'text-nowrap'
            },
            {
                orderable: false,
                className: 'text-center',
                data: function (row) {
                    return {links: row.links, is_active: row.is_active};
                },
                render: function (data) {
                    return [
                        '<div class="dropdown">',
                          '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">',
                            '<span class="sr-only">Actions</span>',
                            '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>',
                            ' <span class="caret"></span>',
                          '</button>',
                          '<ul class="dropdown-menu">',
                            '<li><a class="monitoring-action" href="'+data.links.monitoring+'">Monitoring</a></li>',
                            '<li><a class="config-action" href="'+data.links.config+'">Config</a></li>',
                            '<li role="separator" class="divider"></li>',
                            '<li><a class="edit-action" href="'+data.links._self+'">Edit</a></li>',
                            '<li><a class="reset-password-action" href="'+data.links._self+'">Reset password</a></li>',
                            '<li><a class="active-action" href="'+data.links.active+'">'+(data.is_active ? 'Deactivate' : 'Activate')+'</a></li>',
                            '<li><a class="delete-action" href="'+data.links._self+'">Delete</a></li>',
                          '</ul>',
                        '</div>'
                    ].join('');
                }
            },
        ],
        createdRow: function (row, data) {
            if (!data.is_active) {
                $(row).addClass('text-muted');
            } else {
                $(row).removeClass('text-muted');
            }
        },
        ajax: function (data, callback) {
            $.ajax({
                url: dataurl,
                data: {
                    draw: data.draw,
                    start: data.start,
                    length: data.length,
                    order: columns[data.order[0].column]+','+data.order[0].dir
                },
                dataType: 'json'
            }).done(function (result) {
                callback({
                    draw: result.draw,
                    recordsTotal: result.recordsTotal,
                    recordsFiltered: result.recordsFiltered,
                    data: result.data
                });
            });
        }
    });

    var modalHandlers = {
        success: function (message) {
            table.ajax.reload(null, false);
            notification.success(message);
        },
        failure: function (message) {
            notification.error('<strong>Whoops</strong>, Looks like something went wrong, please try again later.');
        }
    };

    var editModal = EditModal(modals.edit, modalHandlers);
    var resetPasswordModal = ResetPasswordModal(modals.resetPassword, modalHandlers);
    var activationModal = ActivationModal(modals.active, modalHandlers);
    var deleteModal = DeleteModal(modals.delete, modalHandlers);

    container.on('click', '.edit-action', function (e) {
        var row = table.row($(this).closest('tr'));
        editModal.show(row.data());
        e.preventDefault();
    });

    container.on('click', '.reset-password-action', function (e) {
        var row = table.row($(this).closest('tr'));
        resetPasswordModal.show(row.data());
        e.preventDefault();
    });

    container.on('click', '.delete-action', function (e) {
        var row = table.row($(this).closest('tr'));
        deleteModal.show(row.data());
        e.preventDefault();
    });

    container.on('click', '.active-action', function (e) {
        var row = table.row($(this).closest('tr'));
        activationModal.show(row.data());
        e.preventDefault();
    });
};
