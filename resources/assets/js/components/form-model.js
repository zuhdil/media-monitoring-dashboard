function resetErrors(fields) {
    for (var name in fields) {
        fields[name].container.removeClass('has-error');
        fields[name].error.html('');
    }
}

module.exports = function (form) {
    var form = $(form);
    var fields = {};

    form.find('[name]').each(function () {
        var field = $(this);
        var name = field.prop('name');

        fields[name] = {
            input: field,
            container: field.closest('.form-group'),
            error: field.next()
        };
    });

    return {
        fields: fields,
        data: function (key, value) {
            return (value === undefined) ? form.data(key) : form.data(key, value);
        },
        prop: function (key, value) {
            return (value === undefined) ? form.prop(key) : form.prop(key, value);
        },
        submit: function (handler) {
            form.on('submit', handler);
        },
        input: function (name, value) {
            return fields[name].input.val(value);
        },
        focus: function (name) {
            if (fields[name]) {
                fields[name].input.focus();
            }
        },
        serialize: function () {
            return form.serialize();
        },
        reset: function () {
            resetErrors(fields);
            form.trigger('reset');
        },
        errors: function (errors) {
            resetErrors(fields);
            for (var name in errors) {
                if (fields[name]) {
                    fields[name].container.addClass('has-error');
                    fields[name].error.html('<strong>'+errors[name]+'</strong>');
                }
            }
        }
    };
}
