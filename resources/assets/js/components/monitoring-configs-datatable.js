var FormModel = require('./form-model');
var notification = require('./notification');

function DeleteModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));
    var confirmation = container.find('.confirmation');

    container.on('hidden.bs.modal', function () {
        confirmation.html('');
    });

    form.submit(function (e) {
        var config = form.data('config');

        $.ajax({
            type: 'delete',
            url: config.links._self,
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
        }).fail(function (xhr) {
            handler.failure(xhr.statusText);
        }).always(function () {
            container.modal('hide');
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (config) {
            form.data('config', config);
            confirmation.html('Delete <strong>'+config.keyword+'</strong>?');
            container.modal('show');
        }
    };
}

module.exports = function (selector, dataurl, deleteModal) {
    var columns = {'0':'keyword', '1':'started_at', '2':'created_at'};
    var container = $(selector);

    var table = container.DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        order: [[1, 'desc']],
        columns: [
            {
                data: 'keyword'
            },
            {
                data: 'started_at'
            },
            {
                data: 'created_at'
            },
            {
                data: 'links',
                orderable: false,
                className: 'text-center',
                render: function (links) {
                    return [
                        '<a href="'+links._self+'" type="button" class="btn btn-default btn-sm delete-action" title="Delete">',
                        '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>',
                        '</a>'
                    ].join('');
                }
            }
        ],
        ajax: function (data, callback) {
            $.ajax({
                url: dataurl,
                data: {
                    draw: data.draw,
                    start: data.start,
                    length: data.length,
                    order: columns[data.order[0].column]+','+data.order[0].dir
                },
                dataType: 'json'
            }).done(function (result) {
                callback({
                    draw: result.draw,
                    recordsTotal: result.recordsTotal,
                    recordsFiltered: result.recordsFiltered,
                    data: result.data
                });
            });
        }
    });

    var deleteModal = DeleteModal(deleteModal, {
        success: function (message) {
            table.ajax.reload(null, false);
            notification.success(message);
        },
        failure: function (message) {
            notification.error('<strong>Whoops</strong>, Looks like something went wrong, please try again later.');
        }
    });

    container.on('click', '.delete-action', function (e) {
        var row = table.row($(this).closest('tr'));
        deleteModal.show(row.data());
        e.preventDefault();
    });
};
