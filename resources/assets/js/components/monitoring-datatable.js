var DataTableWithChildrow = require('./datatable-with-childrow');
var notification = require('./notification');

module.exports = function (selector, dataUrl, bulkUpdateUrl, categories, queryParams) {
    var categories = JSON.parse(categories);
    var queryParams = JSON.parse(queryParams);
    var container = $(selector);

    function format (data) {
        var categoryLinks = [];
        for (category in categories) {
            var link = '<a class="set-category category-'+category+'" data-category="'+category+'" href="'+data.links._self+'" title="'+categories[category].label+'">'+
                '<span class="label label-default category-'+category+'" style="font-size:100%;">'+categories[category].abbr+'</span>'+
            '</a> ';
            categoryLinks.push(link);
        }

        return '<table>'+
            '<tr>'+
                '<td>Source&nbsp;</td>'+
                '<td> '+data.source+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td>&nbsp;</td>'+
                '<td>'+data.snippet+'</td>'+
            '</tr>'+
            '<tr>'+
                '<td><div class="view"><a href="'+data.url+'" target="_blank">view</a></div></td>'+
                '<td>'+categoryLinks.join('')+'</td>'+
            '</tr>'+
        '</table>';
    }

    var sourceFilter = '';

    var table = DataTableWithChildrow(container.DataTable({
        processing: true,
        serverSide: true,
        searching: false,
        order: [[2, 'desc']],
        columns: [
            {
                data: 'id',
                orderable: false,
                render: function (data) {
                    return '<input type="checkbox" class="snippet" value="'+data+'">';
                }
            },
            {
                data: 'title',
                orderable: false
            },
            {
                data: 'timestamp'
            },
            {
                data: 'category',
                orderable: false,
                defaultContent: '',
                render: function (category) {
                    return (category && category in categories) ?
                        '<span class="label label-default category-'+category+'" style="font-size:100%;">'+categories[category].abbr+'</span>' :
                        '';
                }
            },
            {
                data: null,
                orderable: false,
                defaultContent: '<a href="#" class="childrow-control">&nbsp;</a>'
            }
        ],
        ajax: function (data, callback) {
            $.ajax({
                url: dataUrl,
                data: $.extend({
                    draw: data.draw,
                    start: data.start,
                    length: data.length,
                    order: data.order[0].dir,
                    source: sourceFilter
                }, queryParams),
                dataType: 'json'
            }).done(function (result) {
                callback({
                    draw: result.draw,
                    recordsTotal: result.recordsTotal,
                    recordsFiltered: result.recordsFiltered,
                    data: result.data
                });
            });
        },
        drawCallback: function () {
            this.api().rows().every(function () {
                this.child(format(this.data()), 'details').show();
                $(this.node()).addClass('shown');
            });
        }
    }));

    var processingBanner = $(table.table().container()).find('.dataTables_processing');

    container.on('click', '.set-category', function (e) {
        var anchor = $(this);

        processingBanner.show();

        $.ajax({
            type: 'post',
            url: anchor.prop('href'),
            data: {category: anchor.data('category')},
            dataType: 'json'
        }).done(function (res) {
            setTimeout(function () {
                table.ajax.reload(function () {
                    notification.success(res.message);
                    processingBanner.hide();
                }, false);
            }, 1000);
        }).fail(function () {
            notification.error('<strong>Whoops</strong>, Looks like something went wrong, please try again later.');
            processingBanner.hide();
        });

        e.preventDefault();
        e.stopPropagation();
    });

    var selectSnippets = container.find('.select-snippets');
    var categoryButtons = container.find('.btn-category');

    selectSnippets.on('change', function () {
        var checked = selectSnippets.prop('checked');

        container.find('.snippet').prop('checked', checked);
        categoryButtons.prop('disabled', ! checked);
    });

    container.on('change', '.snippet', function () {
        var countAll = container.find('.snippet').length;
        var countChecked = container.find('.snippet:checked').length;

        selectSnippets.prop('checked', countAll == countChecked);
        selectSnippets.prop('indeterminate', countChecked > 0 && countChecked < countAll);
        categoryButtons.prop('disabled', ! countChecked);
    });

    categoryButtons.on('click', function () {
        processingBanner.show();

        $.ajax({
            type: 'post',
            url: bulkUpdateUrl,
            data: {
                category: this.value,
                snippets: container.find('.snippet:checked').map(function () { return this.value; }).get()
            },
            dataType: 'json'
        }).done(function (res) {
            setTimeout(function () {
                table.ajax.reload(function () {
                    notification.success(res.message);
                    selectSnippets.prop('checked', false);
                    selectSnippets.prop('indeterminate', false);
                    categoryButtons.prop('disabled', true);
                    processingBanner.hide();
                }, false);
            }, 1000);
        }).fail(function () {
            notification.error('<strong>Whoops</strong>, Looks like something went wrong, please try again later.');
            processingBanner.hide();
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        reload: function (source) {
            sourceFilter = source;
            table.ajax.reload();
        }
    };
};
