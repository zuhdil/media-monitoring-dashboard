var container = $('#notification');

module.exports = {
    success: function (message) {
        container.append([
            '<div class="alert alert-success alert-dismissible" role="alert">',
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
            message,
            '</div>'
        ].join(''));
    },
    error: function (message) {
        container.append([
            '<div class="alert alert-danger alert-dismissible" role="alert">',
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
            message,
            '</div>'
        ].join(''));
    }
};
