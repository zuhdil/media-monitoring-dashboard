var FormModel = require('./form-model');
var notification = require('./notification');

function AddModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));

    container.on('hidden.bs.modal', function () {
        form.reset();
    });

    container.on('shown.bs.modal', function () {
        form.focus('username');
    });

    form.submit(function (e) {
        $.ajax({
            type: form.prop('method'),
            url: form.prop('action'),
            data: form.serialize(),
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
            container.modal('hide');
        }).fail(function (xhr) {
            if (xhr.status == 422) {
                var errors = xhr.responseJSON;
                form.errors(errors);
            } else {
                handler.failure(xhr.statusText);
                container.modal('hide');
            }
        });

        e.preventDefault();
        e.stopPropagation();
    });
}

function EditModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));

    container.on('hidden.bs.modal', function () {
        form.reset();
        form.data('user', null);
    });

    container.on('shown.bs.modal', function () {
        form.focus('username');
    });

    form.submit(function (e) {
        var user = form.data('user');

        $.ajax({
            type: 'patch',
            url: user.links._self,
            data: form.serialize(),
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
            container.modal('hide');
        }).fail(function (xhr) {
            if (xhr.status == 422) {
                var errors = xhr.responseJSON;
                form.errors(errors);
            } else {
                handler.failure(xhr.statusText);
                container.modal('hide');
            }
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (user) {
            form.data('user', user);
            form.input('username', user.username);
            form.input('name', user.name);
            container.modal('show');
        }
    };
}

function ResetPasswordModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));

    container.on('hidden.bs.modal', function () {
        form.reset();
        form.data('user', null);
    });

    container.on('shown.bs.modal', function () {
        form.focus('password');
    });

    form.submit(function (e) {
        var user = form.data('user');

        $.ajax({
            type: 'patch',
            url: user.links._self,
            data: form.serialize(),
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
            container.modal('hide');
        }).fail(function (xhr) {
            if (xhr.status == 422) {
                var errors = xhr.responseJSON;
                form.errors(errors);
            } else {
                handler.failure(xhr.statusText);
                container.modal('hide');
            }
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (user) {
            form.data('user', user);
            container.modal('show');
        }
    };
}

function DeleteModal(selector, handler) {
    var container = $(selector);
    var form = FormModel(container.find('form'));
    var confirmation = container.find('.confirmation');

    container.on('hidden.bs.modal', function () {
        confirmation.html('');
    });

    form.submit(function (e) {
        var user = form.data('user');

        $.ajax({
            type: 'delete',
            url: user.links._self,
            dataType: 'json'
        }).done(function (result) {
            handler.success(result.message);
        }).fail(function (xhr) {
            handler.failure(xhr.statusText);
        }).always(function () {
            container.modal('hide');
        });

        e.preventDefault();
        e.stopPropagation();
    });

    return {
        show: function (user) {
            form.data('user', user);
            confirmation.html('Delete <strong>'+user.username+'</strong>?');
            container.modal('show');
        }
    };
}

module.exports = function (selector, dataurl, modals) {
    var columns = {'0':'username', '1':'name', '3':'created_at'};
    var container = $(selector);

    var table = container.DataTable({
        processing: true,
        serverSide: true,
        searching: true,
        columns: [
            {data: 'username'},
            {data: 'name'},
            {data: 'created_at', className: 'text-nowrap'},
            {
                data: 'links',
                orderable: false,
                className: 'text-center',
                render: function (links) {
                    return [
                        '<div class="dropdown">',
                        '<button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">',
                        '<span class="sr-only">Actions</span>',
                        '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>',
                        ' <span class="caret"></span>',
                        '</button>',
                        '<ul class="dropdown-menu">',
                        '<li><a class="edit-action" href="'+links._self+'">Edit</a></li>',
                        '<li><a class="reset-password-action" href="'+links._self+'">Reset password</a></li>',
                        '<li><a class="delete-action" href="'+links._self+'">Delete</a></li>',
                        '</ul>',
                        '</div>'
                    ].join('');
                }
            }
        ],
        ajax: function (data, callback) {
            $.ajax({
                url: dataurl,
                data: {
                    draw: data.draw,
                    start: data.start,
                    length: data.length,
                    order: columns[data.order[0].column]+','+data.order[0].dir
                },
                dataType: 'json'
            }).done(function (result) {
                callback({
                    draw: result.draw,
                    recordsTotal: result.recordsTotal,
                    recordsFiltered: result.recordsFiltered,
                    data: result.data
                });
            });
        }
    });

    var modalHandlers = {
        success: function (message) {
            table.ajax.reload(null, false);
            notification.success(message);
        },
        failure: function (message) {
            notification.error('<strong>Whoops</strong>, Looks like something went wrong, please try again later.');
        }
    };

    AddModal(modals.add, modalHandlers);

    var editModal = EditModal(modals.edit, modalHandlers);
    var resetPasswordModal = ResetPasswordModal(modals.resetPassword, modalHandlers);
    var deleteModal = DeleteModal(modals.delete, modalHandlers);

    container.on('click', '.edit-action', function (e) {
        var row = table.row($(this).closest('tr'));
        editModal.show(row.data());
        e.preventDefault();
    });

    container.on('click', '.reset-password-action', function (e) {
        var row = table.row($(this).closest('tr'));
        resetPasswordModal.show(row.data());
        e.preventDefault();
    });

    container.on('click', '.delete-action', function (e) {
        var row = table.row($(this).closest('tr'));
        deleteModal.show(row.data());
        e.preventDefault();
    });

    return table;
};
