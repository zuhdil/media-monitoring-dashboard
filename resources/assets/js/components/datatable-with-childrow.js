module.exports = function (datatable) {
    $(datatable.table().body()).on('click', '.childrow-control', function (e) {
        var tr = $(this).closest('tr');
        var row = datatable.row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            row.child.show();
            tr.addClass('shown');
        }

        e.preventDefault();
        e.stopPropagation();
    });

    return datatable;
};
