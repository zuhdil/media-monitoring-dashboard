
require('./bootstrap');

window.ClientsDataTable = require('./components/clients-datatable');
window.MonitoringConfigsDataTable = require('./components/monitoring-configs-datatable');
window.MonitoringDataTable = require('./components/monitoring-datatable');
window.UserDataTable = require('./components/users-datatable');
