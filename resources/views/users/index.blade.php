@extends('layout')

@section('content')
  <div class="page-header">
    <h1>User List</h1>
  </div>

  <p class="pull-right">
    <button class="btn btn-primary" data-toggle="modal" data-target="#user-add-modal">Add User</button>
  </p>
  <div id="user-add-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="user-add-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form method="post" action="{{ route('users') }}">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="user-add-modal-label">Add user</h4>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label for="user-add-username">Username</label>
              <input id="user-add-username" class="form-control" type="text" name="username" placeholder="Username">
              <span class="help-block"></span>
            </div>

            <div class="form-group">
              <label for="user-add-name">Name</label>
              <input id="user-add-name" class="form-control" type="text" name="name" placeholder="Name">
              <span class="help-block"></span>
            </div>

            <div class="form-group">
              <label for="user-add-password">Password</label>
              <input id="user-add-password" class="form-control" type="password" name="password">
              <span class="help-block"></span>
            </div>

            <div class="form-group">
              <label for="user-add-password-confirm">Confirm password</label>
              <input id="user-add-password-confirm" class="form-control" type="password" name="password_confirmation">
              <span class="help-block"></span>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <table id="user-list" class="table">
    <thead>
      <tr>
        <th>Username</th>
        <th>Name</th>
        <th>Created at</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>

  <div class="modal fade" id="user-edit-modal" tabindex="-1" role="dialog" aria-labelledby="user-edit-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="user-edit-modal-label">Edit user</h4>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label for="user-edit-username">Username</label>
              <input type="text" class="form-control" name="username" id="user-edit-username" value="" placeholder="Username">
              <span class="help-block"></span>
            </div>

            <div class="form-group">
              <label for="user-edit-name">Name</label>
              <input type="text" class="form-control" name="name" id="user-edit-name" value="" placeholder="Name">
              <span class="help-block"></span>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="user-password-modal" tabindex="-1" role="dialog" aria-labelledby="user-password-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="user-password-modal-label">Reset user's password</h4>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label for="user-password-password">Password</label>
              <input type="password" class="form-control" name="password" id="user-password-password">
              <span class="help-block"></span>
            </div>

            <div class="form-group">
              <label for="user-edit-username">Confirm password</label>
              <input type="password" class="form-control" name="password_confirmation" id="user-password-confirmation">
              <span class="help-block"></span>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="user-delete-modal" tabindex="-1" role="dialog" aria-labelledby="user-delete-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="user-delete-modal-label">Delete user</h4>
          </div>
          <div class="modal-body">
            <p class="confirmation"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function () {
      var table = UserDataTable('#user-list', "{{ route('users') }}", {
        add: '#user-add-modal',
        edit: '#user-edit-modal',
        resetPassword: '#user-password-modal',
        delete: '#user-delete-modal'
      })
    });
  </script>
@endsection
