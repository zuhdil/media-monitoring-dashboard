@extends('layout')

@section('content')
  <div class="login-container">
    <div class="loginbox">
      <div class="login-heading">
        <h1>LOGIN</h1>
        <p>
          Please login into your account
        </p>
      </div>
      <form class="form" action="{{ route('login') }}" role="form" method="post">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
          <input id="username" class="form-control" type="text" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus>
          @if ($errors->has('username'))
            <span class="help-block">
              <strong>{{ $errors->first('username') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group">
          <input id="password" class="form-control" type="password" name="password" placeholder="Password" required>
          @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group">
          <div class="checkbox">
            <label>
              <input type="checkbox" name="remember"> Remember Me
            </label>
          </div>
        </div>
        <br/>
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-block btn-lg">
            Login
          </button>
        </div>
      </form>

    </div>
  </div>
@endsection
