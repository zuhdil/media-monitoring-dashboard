@extends('layout')

@section('content')
  <div class="page-header">
    <h1>Client List</h1>
  </div>

  <p class="pull-right">
    <a class="btn btn-primary btn-lg" href="{{ route('clients.create') }}">Add Client</a>
  </p>

  <table id="client-list" class="table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Discovery Keywords</th>
        <th>Created at</th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>


  <div class="modal fade" id="client-edit-modal" tabindex="-1" role="dialog" aria-labelledby="client-edit-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="client-edit-modal-label">Edit client</h4>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label for="client-edit-name">Client name</label>
              <input type="text" class="form-control" name="name" id="client-edit-name" value="" placeholder="Client name">
              <span class="help-block"></span>
            </div>

            <div class="form-group">
              <label for="client-edit-username">Username</label>
              <input type="text" class="form-control" name="username" id="client-edit-username" value="" placeholder="Username">
              <span class="help-block"></span>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="modal fade" id="client-password-modal" tabindex="-1" role="dialog" aria-labelledby="client-password-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="client-password-modal-label">Reset client's password</h4>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label for="client-password-password">Password</label>
              <input type="password" class="form-control" name="password" id="client-password-password">
              <span class="help-block"></span>
            </div>

            <div class="form-group">
              <label for="client-edit-username">Confirm password</label>
              <input type="password" class="form-control" name="password_confirmation" id="client-password-confirmation">
              <span class="help-block"></span>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="client-active-modal" tabindex="-1" role="dialog" aria-labelledby="client-active-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="client-active-modal-label"></h4>
          </div>
          <div class="modal-body">
            <p class="confirmation"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="client-delete-modal" tabindex="-1" role="dialog" aria-labelledby="client-delete-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="client-delete-modal-label">Delete client</h4>
          </div>
          <div class="modal-body">
            <p class="confirmation"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function() {
      ClientsDataTable('#client-list', "{{ route('clients') }}", {
        edit: '#client-edit-modal',
        resetPassword: '#client-password-modal',
        active: '#client-active-modal',
        delete: '#client-delete-modal'
      });
    });
  </script>
@endsection
