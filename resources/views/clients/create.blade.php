@extends('layout')

@section('content')
  <div class="page-header">
    <h1>Add Client</h1>
  </div>

  <form class="form" role="form" method="post" action="{{ route('clients') }}">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="input-client-name">Client name</label>
      <input type="text" class="form-control" name="name" id="input-client-name" value="{{ old('name') }}" placeholder="Client name">
      @if ($errors->has('name'))
        <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
        </span>
      @endif
    </div>
    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
      <label for="input-username">Username</label>
      <input type="text" class="form-control" name="username" id="input-username" value="{{ old('username') }}" placeholder="Username">
      @if ($errors->has('username'))
        <span class="help-block">
          <strong>{{ $errors->first('username') }}</strong>
        </span>
      @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
      <label for="input-password">Password</label>
      <input type="password" class="form-control" name="password" id="input-password">
      @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
      @endif
    </div>
    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
      <label for="input-password-confirmation">Confirm password</label>
      <input type="password" class="form-control" name="password_confirmation" id="input-password-confirmation">
      @if ($errors->has('password_confirmation'))
        <span class="help-block">
          <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
      @endif
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
    <!-- button type="submit" class="btn btn-default">Save &amp; Continue</button -->
    <!-- button type="submit" class="btn btn-default">Save &amp; Exit</button -->
    <!-- button type="reset" class="btn btn-default">Cancel</button -->
  </form>
@endsection
