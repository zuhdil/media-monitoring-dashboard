@extends('layout')

@section('content')
  <div class="page-header">
    <h1>Dashboard</h1>
  </div>

  <p>You are logged in as {{ $user->username }} ({{ get_class($user) }})!</p>
@endsection
