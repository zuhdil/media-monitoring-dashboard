@extends('layout')

@section('content')
  <div class="page-header">
    <h1>{{ $title }}</h1>
  </div>


  <div class="btn-group">
    <button class="btn btn-default" data-toggle="modal" data-target="#daily-report-modal">
      Daily Report
    </button>
    <button class="btn btn-default" data-toggle="modal" data-target="#monthly-report-modal">
      Monthly Report
    </button>
  </div>

  <div id="daily-report-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="daily-report-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form id="daily-report-form" action="{{ route('reports.daily', $client->username) }}" method="get">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 id="daily-report-modal-label" class="modal-title">Daily report</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="daily-report-date">Date</label>
              <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-end-date="{{ (new DateTimeImmutable('-1 day'))->format('Y-m-d') }}">
                <input id="daily-report-date" name="date" class="form-control" type="text" value="{{ (new DateTimeImmutable('-1 day'))->format('Y-m-d') }}">
                <div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div id="monthly-report-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="monthly-report-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form id="monthly-report-form" action="{{ route('reports.monthly', $client->username) }}" method="get">
          <div class="modal-header">
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 id="monthly-report-modal-label" class="modal-title">Monthly report</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="monthly-report-month">Month</label>
              <div class="input-group date" data-provide="datepicker" data-date-min-view-mode="months" data-date-format="yyyy-mm" data-date-end-date="{{ (new DateTimeImmutable('last month'))->format('Y-m') }}">
                <input id="monthly-report-month" name="month" class="form-control" type="text" value="{{ (new DateTimeImmutable('last month'))->format('Y-m') }}">
                <div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-default" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div class="pull-right">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#advance-search-modal">
      Advance Search
    </button>
  </div>

  <div id="advance-search-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="advance-search-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form id="advance-search-form" action="{{ route('monitoring.search', $client->username) }}" method="get">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 id="advance-search-modal-label" class="modal-title">Advance search</h4>
          </div>
          <div class="modal-body">

            <div class="form-group">
              <label for="advance-search-keyword">Keyword</label>
              <input id="advance-search-keyword" name="q" class="form-control" type="text" placeholder="Keyword">
            </div>

            <div class="form-group">
              <label for="advance-search-date-range">Date range</label>
              <div class="input-group input-daterange" data-provide="datepicker" data-date-format="yyyy-mm-dd" data-date-end-date="{{ (new DateTimeImmutable('now'))->format('Y-m-d') }}">
                <input id="advance-search-date-range" name="from" type="text" class="form-control" value="{{ (new DateTimeImmutable('now'))->format('Y-m-d') }}">
                <span class="input-group-addon">to</span>
                <input type="text" class="form-control" name="to" value="{{ (new DateTimeImmutable('now'))->format('Y-m-d') }}">
              </div>
            </div>

            <div class="form-group">
              <label for="advance-search-sources">Source</label>
              <select id="advance-search-sources" name="sources[]" class="form-control select-filter" multiple>
                @foreach (config('scraper.sources') as $key => $source)
                  <option value="{{ $key }}">{{ $source['label'] }}</option>
                @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="advance-search-category">Category</label>
              <select id="advance-search-category" name="categories[]" class="form-control select-filter" multiple>
                @foreach (config('scraper.categories') as $key => $category)
                  <option value="{{ $key }}">{{ $category['abbr'] }} - {{ $category['label'] }}</option>
                @endforeach
              </select>
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <br class="clearfix">
  <br>


  <div class="pull-right">
    <label for="">Filter:
      <select id="monitoring-source-filter"{{ ($disableSourceFilter) ? ' disabled' : '' }}>
        <option value="">Select all</option>
        @foreach (config('scraper.sources') as $key => $source)
        <option value="{{ $key }}">{{ $source['label'] }}</option>
        @endforeach
      </select>
    </label>
  </div>

  <table id="monitoring" class="table datatable-with-childrow">
    <thead>
      <tr>
        <th width="80">&nbsp</th>
        <th>Snippet</th>
        <th>Date</th>
        <th>Status</th>
        <th width="20">&nbsp;</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
        <td><input type="checkbox" class="select-snippets"></td>
        <td colspan="4">
          @foreach (config('scraper.categories') as $category => $categoryInfo)
            <button class="btn btn-default btn-category category-{{ $category }}" type="button" value="{{ $category }}" disabled>
              <strong>{{ $categoryInfo['abbr'] }}</strong>
            </button>
          @endforeach
        </td>
      </tr>
    </tfoot>
  </table>
@endsection

@section('scripts')
  <script>
    $(document).ready(function () {
      var table = MonitoringDataTable(
        '#monitoring',
        '{{ $dataUrl }}',
        '{{ route("snippets.bulk_update", $client->username) }}',
        '{!! json_encode(config("scraper.categories"), JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_FORCE_OBJECT) !!}',
        '{!! json_encode($queryParams, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_FORCE_OBJECT) !!}'
      );

      $('#monitoring-source-filter').multiselect({
        onChange: function (option, checked) {
          table.reload(option.val());
        }
      });

      $('.select-filter').multiselect({
        includeSelectAllOption: true,
      });
    });
  </script>
@endsection
