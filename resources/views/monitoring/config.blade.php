@extends('layout')

@section('content')
  <div class="page-header">
    <h1>{{ $client->name }}, Monitoring Configuration</h1>
  </div>
  <div class="row">
    <div class="col-sm-3 sidebar">
      <form class="form" action="{{ route('clients.config', $client->username) }}" role="form" method="post" >
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('keyword') ? ' has-error' : '' }}">
          <label for="input-keyword">Keyword</label>
          <input id="input-keyword" class="form-control" name="keyword" type="text" value="{{ old('keyword') }}" placeholder="Keyword">
          @if ($errors->has('keyword'))
            <span class="help-block">
              <strong>{{ $errors->first('keyword') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('started_at') ? ' has-error' : '' }}">
          <label for="input-start-date">Start date</label>
          <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
            <input id="input-start-date" class="form-control" name="started_at" type="text" value="{{ old('started_at') }}" placeholder="yyyy-mm-dd">
            <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
            </div>
          </div>
          @if ($errors->has('started_at'))
            <span class="help-block">
              <strong>{{ $errors->first('started_at') }}</strong>
            </span>
          @endif
        </div>

        <button class="btn btn-primary" type="submit">Submit</button>
      </form>
    </div>
    <div class="col-sm-9">
      <table id="monitoring-configs" class="table">
        <thead>
          <tr>
            <th>Keyword</th>
            <th>Start date</th>
            <th>Created at</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>  

  <div class="modal fade" id="config-delete-modal" tabindex="-1" role="dialog" aria-labelledby="config-delete-modal-label">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="config-delete-modal-label">Delete config</h4>
          </div>
          <div class="modal-body">
            <p class="confirmation"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function () {
      MonitoringConfigsDataTable('#monitoring-configs', "{{ route('clients.config', $client->username) }}", '#config-delete-modal');
    });
  </script>
@endsection
