@extends('base')

@section('body')
  @if (Auth::check())
  <nav class="navbar header navbar-fixed-top">
    <div class="container-fluid">

      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="{{ url('/') }}"><i class="glyphicon glyphicon-stats"></i> Media Monitoring</a>
      </div>

      <div class="collapse navbar-collapse" id="main-navbar-collapse">
        <ul class="nav navbar-nav">
          @if (Auth::user() instanceof App\User)

            @include('nav', [
              'nav' => [
                ['label' => 'Clients', 'url' => route('clients')],
                ['label' => 'Users', 'url' => route('users')]
              ],
              'current' => isset($page) ? $page : '',
            ])

          @else
            &nbsp;
          @endif
        </ul>

        <ul class="nav navbar-nav navbar-right">
          @if (Auth::check())

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::user()->name }} <span class="caret"></span>
              </a>

              <ul class="dropdown-menu" role="menu">
                <li>
                  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
              </ul>
            </li>

          @else
            &nbsp;
          @endif
        </ul>
      </div>

    </div>
  </nav>
  @endif

  <div id="notification">
    @if (session('status'))
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ session('status') }}
      </div>
    @endif
    @if ($errors->has(0))
    <div class="alert alert-danger">
        <strong>Whoops!</strong> {{ $errors->first(0) }}
    </div>
    @endif
  </div>

  <div class="wrapper">
    <div class="container-fluid">
      @yield('content')
    </div>
  </div>

@endsection
