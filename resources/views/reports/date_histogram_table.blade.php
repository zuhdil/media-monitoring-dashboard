<div class="panel panel-default">
  <div class="panel-heading">
    <h4>Sensor: {{ $sensor }} | Date: {{ $startDate->format('Y-m-d') }} - {{ $endDate->format('Y-m-d') }}</h4>
  </div>

  <table class="table">
    <thead>
      <tr>
        @foreach($data['columns'] as $column)
          <th>{{ $column['label'] }}</th>
        @endforeach
      </tr>
    </thead>
    <tfoot>
      <td colspan={{ count($data['columns'])-1 }}>&nbsp;</td>
      <td class="text-right"><strong>{{ $data['count'] }}</strong></td>
    </tfoot>
    <tbody>
      @forelse($data['data'] as $row)
        <tr>
          @foreach($row as $column)
            @if($column instanceof DateTimeInterface)
              <td class="text-nowrap">{{ $column->format($dateDataFormat) }}</td>
            @else
              <td class="text-right">{{ $column }}</td>
            @endif
          @endforeach
        </tr>
      @empty
        <tr>
          <td colspan="{{ count($data['columns']) }}" class="text-center">No data available</td>
        </tr>
      @endforelse
    </tbody>
  </table>
</div>
