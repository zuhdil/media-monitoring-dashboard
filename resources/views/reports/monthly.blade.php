@extends('base')

@section('body')
  <div class="container">
    <div class="page-header">
      <h1>{{ $client->name }} monthly report, {{ $startDate->format('F Y') }}.</h1>
    </div>
    <div>
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
          <a href="#by-date" aria-controls="by-date" role="tab" data-toggle="tab">All event weekly</a>
        </li>
        <li role="presentation">
          <a href="#by-hour" aria-controls="by-hour" role="tab" data-toggle="tab">All events hourly</a>
        </li>
        <li role="presentation">
          <a href="#top-10" aria-controls="top-10" role="tab" data-toggle="tab">Top 10 alerts</a>
        </li>
      </ul>

      <br>

      <div class="tab-content">
        <div class="tab-pane active" role="tabpanel" id="by-date">
          @include('reports.date_histogram_table', [
            'sensor' => 'All sensor',
            'startDate' => $startDate,
            'endDate' => $endDate,
            'data' => $byDate['all'],
            'dateDataFormat' => 'Y-m-d',
          ])

          @foreach($byDate['by_source'] as $source)
            @include('reports.date_histogram_table', [
              'sensor' => $source['label'],
              'startDate' => $startDate,
              'endDate' => $endDate,
              'data' => $source['data'],
              'dateDataFormat' => 'Y-m-d',
            ])
          @endforeach
        </div>

        <div class="tab-pane" role="tabpanel" id="by-hour">
          @include('reports.date_histogram_table', [
            'sensor' => 'All sensor',
            'startDate' => $startDate,
            'endDate' => $endDate,
            'data' => $byHour['all'],
            'dateDataFormat' => 'Y-m-d H:i:s',
          ])

          @foreach($byHour['by_source'] as $source)
            @include('reports.date_histogram_table', [
              'sensor' => $source['label'],
              'startDate' => $startDate,
              'endDate' => $endDate,
              'data' => $source['data'],
              'dateDataFormat' => 'Y-m-d H:i:s',
            ])
          @endforeach
        </div>

        <div class="tab-pane" role="tabpanel" id="top-10">
          <h4>Sensor: All sensor | Date: {{ $startDate->format('Y-m-d') }} - {{ $endDate->format('Y-m-d') }}</h4>

          @foreach($byCategory['by_category'] as $category)
            <div class="panel panel-default">
              <div class="panel-heading"><h4>{{ $category['label'] }}</h4></div>
              <table class="table">
                <thead>
                  <tr>
                    <th>Alert type</th>
                    <th>Occurance</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <td>&nbsp</td>
                    <td class="text-right"><strong>{{ $category['count'] }}</strong></td>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($category['by_source'] as $source)
                  <tr>
                    <td>{{ $source['label'] }}</td>
                    <td class="text-right">{{ $source['count'] }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          @endforeach
        </div>

      </div>
    </div>

  </div>
@endsection
