@extends('base')

@section('body')
  <div class="container">

    <div class="page-header">
      <h1>{{ $title }}, {{ $date->format('j F Y') }}</h1>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><h4>Event Summary Pie Chart</h4></div>
      <div class="panel-body">
        <div id="by-category-chart" class="col-md-6" style="height:500px;"></div>
        <div id="by-source-chart" class="col-md-6" style="height:500px;"></div>
      </div>
    </div>

    <div class="panel panel-default">
      <div class="panel-heading"><h4>Events Summary List</h4></div>
      <table class="table">
        <thead>
          <tr>
            <th>&nbsp;</th>
            <th>Severity</th>
            <th>Total</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th colspan="2" class="text-right">&nbsp;</th>
            <th>{{ $summaryTotal }}</th>
          </tr>
        </tfoot>
        <tbody>
          @foreach ($summaryByCategory as $category => $categoryInfo)
            <tr>
              <td class="category-{{ $category }}">&nbsp</td>
              <td>{{ $categoryInfo['label'] }}</td>
              <td>{{ $categoryInfo['count'] }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    @foreach ($samples as $source => $feeds)
    <div class="panel panel-default">
      <div class="panel-heading"><h4>{{ $source }}</h4></div>
      <table class="table table-striped events-sample">
        <tbody>
          @forelse ($feeds as $feed)
            <tr>
              <td class="text-center category-label category-{{ $feed['category'] }}"><strong>{{ $categories[$feed['category']]['abbr'] }}</strong></td>
              <td>{{ $feed['url'] }}</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>{{ $feed['title'] }}</td>
            </tr>
          @empty
            <tr>
              <td class="text-center">No data available</td>
            </tr>
          @endforelse
        </tbody>
      </table>
    </div>
    @endforeach

  </div>
@endsection

@section('scripts')
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(function () {
      var byCategoryChart = new google.visualization.PieChart(document.getElementById('by-category-chart'));
      byCategoryChart.draw(google.visualization.arrayToDataTable([
        ['Severity', 'Total'],
        {!! implode(',', array_map(function($category) { return sprintf("['%s', %d]", $category['label'], $category['count']); }, $summaryByCategory)) !!}
      ]), {
        title: 'Event Summary By Severity',
        is3D: true
      });

      var bySourceChart = new google.visualization.PieChart(document.getElementById('by-source-chart'));
      bySourceChart.draw(google.visualization.arrayToDataTable([
        ['Source', 'Total'],
        {!! implode(',', array_map(function($category) { return sprintf("['%s', %d]", $category['label'], $category['count']); }, $summaryBySource)) !!}
      ]), {
        title: 'Event Summary By Source',
        is3D: true
      });
    });
  </script>
@endsection
