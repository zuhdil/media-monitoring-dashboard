@foreach ($nav as $item)
  <li{!! ($current == $item['label']) ? ' class="active"':'' !!}>
    <a href="{{ $item['url'] }}">{{ $item['label']}}
      @if ($current == $item['label'])
        <span class="sr-only">(current)</span>
      @endif
    </a>
  </li>
@endforeach
