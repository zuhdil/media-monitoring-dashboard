<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;
use DateTimeImmutable;

class ClientCategorizedSnippetsFeatureTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function open_client_categorized_snippets_page()
    {
        factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring.categorized', 'foo'))
            ->assertResponseOk()
            ->seePageIs(route('monitoring.categorized', 'foo'));
    }

    /**
     * @test
     */
    public function open_client_categorized_snippets_for_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring.categorized', 'foo'));
    }

    /**
     * @test
     */
    public function only_show_recent_snippets_that_has_category()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-10 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => '-20 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
            $fixture->make([
                'title' => '-30 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-30 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.categorized', 'foo'))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [['title' => '-10 hour']]
            ]);
    }

    /**
     * @test
     */
    public function show_only_snippets_owned_by_specific_client()
    {
        $client1 = factory(Client::class)->create(['username' => 'foo']);
        $client2 = factory(Client::class)->create(['username' => 'bar']);

        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'owned by foo',
                'category' => 'safe-content',
                'owner' => $client1->id,
                'timestamp' => new DateTimeImmutable('now'),
            ]),
            $fixture->make([
                'title' => 'owned by bar',
                'category' => 'safe-content',
                'owner' => $client2->id,
                'timestamp' => new DateTimeImmutable('now'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.categorized', 'foo'))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [['title' => 'owned by foo']]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_order_by_date_asc()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-10 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => '-15 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-15 hour'),
            ]),
            $fixture->make([
                'title' => '-20 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.categorized', ['foo', 'order' => 'asc']))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [
                    ['title' => '-20 hour'],
                    ['title' => '-15 hour'],
                    ['title' => '-10 hour'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_paginate_results()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-10 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => '-15 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-15 hour'),
            ]),
            $fixture->make([
                'title' => '-20 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.categorized', ['foo', 'start'=> 1, 'length' => 1]))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [['title' => '-15 hour']]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_filter_by_source()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1',
                'category' => 'safe-content',
                'source' => 'twitter.com',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2',
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-15 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'category' => 'safe-content',
                'source' => 'www.bing.com',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.categorized', ['foo', 'source' => 'google']))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [['title' => 'snippet 2']]
            ]);
    }
}
