<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;

class SnippetUpdateFeatureTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function request_snippet_update_path_of_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);
        $this->expectExceptionMessageRegExp('/Received status code \[404\]/');

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->makeRequest('post', route('snippets.update', ['foo', '1']));
    }

    /**
     * @test
     */
    public function request_snippet_update_path_with_nonexisted_snippet_should_throw_exception()
    {
        $this->expectException(HttpException::class);
        $this->expectExceptionMessageRegExp('/Received status code \[404\]/');

        factory(Client::class)->create(['username' => 'foo']);
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->makeRequest('post', route('snippets.update', ['foo', '1']));
    }

    /**
     * @test
     */
    public function request_snippet_update_path_should_update_snippet_category()
    {
        $dao = $this->getFixture()->getDAO();
        $snippet = $this->getFixture()->make();
        $dao->bulkIndex([$snippet], true);

        factory(Client::class)->create(['username' => 'foo']);
        $user = factory(User::class)->create();

        $this->actingAs($user)->makeRequest(
            'post',
            route('snippets.update', ['foo', $snippet['id']]),
            ['category' => 'safe-content']
        );

        $result = $dao->get($snippet['id']);
        $this->assertEquals('safe-content', $result['category']);
    }
}
