<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use App\Client;

class UserFeatureTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function open_user_list_page()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('users'))
            ->assertResponseOk()
            ->seePageIs(route('users'));
    }

    /**
     * @test
     */
    public function get_user_list_data()
    {
        factory(User::class, 3)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('users'))
            ->seeJsonStructure([
                'draw',
                'recordsTotal',
                'recordsFiltered',
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'username',
                        'created_at',
                        'links' => ['_self']
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function create_new_user()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('users'), [
                'name' => 'john doe',
                'username' => 'johndoe',
                'password' => '123456',
                'password_confirmation' => '123456',
            ])
            ->seeInDatabase('users', [
                'name' => 'john doe',
                'username' => 'johndoe',
            ]);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_user_using_empty_data()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('users'), [
                'name' => ' ',
                'username' => ' ',
                'password' => ' ',
                'password_confirmation' => ' ',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_user_using_invalid_password_confirmation()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('users'), [
                'name' => 'john doe',
                'username' => 'johndoe',
                'password' => '123456',
                'password_confirmation' => '123455',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_user_using_existing_username()
    {
        factory(User::class)->create(['username' => 'johndoe']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('users'), [
                'name' => 'john doe',
                'username' => 'johndoe',
                'password' => '123456',
                'password_confirmation' => '123456',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_user_using_existing_client_username()
    {
        factory(Client::class)->create(['username' => 'johndoe']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('users'), [
                'name' => 'john doe',
                'username' => 'johndoe',
                'password' => '123456',
                'password_confirmation' => '123456',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function delete_user()
    {
        factory(User::class)->create(['username' => 'johndoe']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('delete', route('users.self', 'johndoe'))
            ->missingFromDatabase('users', ['username' => 'johndoe']);
    }

    /**
     * @test
     */
    public function update_user_should_failed_given_empty_data()
    {
        $target = factory(User::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'name' => ' ',
                'username' => ' ',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function update_user_username_should_failed_given_username_already_used_by_other_user()
    {
        factory(User::class)->create(['username' => 'foo']);
        $target = factory(User::class)->create(['username' => 'bar']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'name' => 'update',
                'username' => 'foo',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function update_user_username_should_failed_given_username_already_used_by_client()
    {
        factory(Client::class)->create(['username' => 'foo']);
        $target = factory(User::class)->create(['username' => 'bar']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'name' => 'update',
                'username' => 'foo',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function update_user_should_update_database_given_valid_input()
    {
        $target = factory(User::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'name' => 'john doe',
                'username' => 'johndoe',
            ])
            ->seeInDatabase('users', [
                'name' => 'john doe',
                'username' => 'johndoe',
            ]);
    }

    /**
     * @test
     */
    public function reset_password_should_failed_given_empty_value()
    {
        $target = factory(User::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'password' => ' ',
                'password_confirmation' => ' ',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function reset_password_should_failed_given_invalid_confirmation()
    {
        $target = factory(User::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'password' => 'opensesame',
                'password_confirmation' => 'abracadabra',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function reset_password_should_update_user_password()
    {
        $target = factory(User::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'password' => 'opensesame',
                'password_confirmation' => 'opensesame',
            ]);

        $updated = User::find($target->id);
        $this->assertTrue($this->app['hash']->check('opensesame', $updated->password));
    }

    /**
     * @test
     */
    public function should_ignore_username_if_username_input_not_changed()
    {
        $target = factory(User::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('users.self', $target->username), [
                'name' => 'john doe',
                'username' => 'foo',
            ])
            ->seeInDatabase('users', [
                'name' => 'john doe',
                'username' => 'foo',
            ]);
    }
}
