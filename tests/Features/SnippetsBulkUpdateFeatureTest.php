<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;

class SnippetsBulkUpdateFeatureTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function request_snippet_bulk_update_path_of_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);
        $this->expectExceptionMessageRegExp('/Received status code \[404\]/');

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->makeRequest('post', route('snippets.bulk_update', 'foo'));
    }

    /**
     * @test
     */
    public function request_snippets_bulk_update_path_should_update_snippets_category()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();

        $snippet1 = $fixture->make(['title' => 'snippet 1']);
        $snippet2 = $fixture->make(['title' => 'snippet 2']);
        $dao->bulkIndex([$snippet1, $snippet2], true);

        factory(Client::class)->create(['username' => 'foo']);
        $user = factory(User::class)->create();

        $this->actingAs($user)->makeRequest(
            'post',
            route('snippets.bulk_update', 'foo'),
            ['category' => 'safe-content', 'snippets' => [$snippet1['id'], $snippet2['id']]]
        );

        $result1 = $dao->get($snippet1['id']);
        $result2 = $dao->get($snippet2['id']);

        $this->assertEquals('safe-content', $result1['category']);
        $this->assertEquals('safe-content', $result2['category']);
    }
}
