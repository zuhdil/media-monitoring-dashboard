<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;
use DateTimeImmutable;

class ClientMonitoringSearchFeatureTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function open_client_monitoring_search_page()
    {
        factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring.search', 'foo'))
            ->assertResponseOk()
            ->seePageIs(route('monitoring.search', 'foo'));
    }

    /**
     * @test
     */
    public function open_client_monitoring_search_for_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring.search', 'foo'));
    }

    /**
     * @test
     */
    public function show_categorized_snippets_between_daterange()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-1 day',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-1 day'),
            ]),
            $fixture->make([
                'title' => '-2 day',
                'category' => null,
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-2 day'),
            ]),
            $fixture->make([
                'title' => '-3 day',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-3 day'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-2 day'))->format('Y-m-d'),
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [
                    ['title' => '-3 day'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function show_recent_categorized_snippets()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-20 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
            $fixture->make([
                'title' => '-1 day',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-1 day'),
            ]),
            $fixture->make([
                'title' => '-2 day',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-2 day'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('now'))->format('Y-m-d'),
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [
                    ['title' => '-20 hour'],
                    ['title' => '-1 day'],
                    ['title' => '-2 day'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function show_only_snippets_owned_by_specific_client()
    {
        $client1 = factory(Client::class)->create(['username' => 'foo']);
        $client2 = factory(Client::class)->create(['username' => 'bar']);

        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'owned by foo',
                'category' => 'safe-content',
                'owner' => $client1->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => 'owned by bar',
                'category' => 'safe-content',
                'owner' => $client2->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ])
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [
                    ['title' => 'owned by foo'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_order_by_date_asc()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-25 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => '-36 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-36 hour'),
            ]),
            $fixture->make([
                'title' => '-49 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-49 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
                'order' => 'asc'
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [
                    ['title' => '-49 hour'],
                    ['title' => '-36 hour'],
                    ['title' => '-25 hour'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_paginate_results()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-25 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => '-36 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-36 hour'),
            ]),
            $fixture->make([
                'title' => '-49 hour',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-49 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
                'start' => 1,
                'length' => 1,
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [['title' => '-36 hour']]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_search_by_keyword()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1 lorem',
                'snippet' => 'ipsum',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2 ipsum',
                'snippet' => 'ipsum dolor',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-26 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'snippet' => 'dolor sit amet',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-27 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
                'q' => 'lorem amet',
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 2,
                'data' => [
                    ['title' => 'snippet 1 lorem'],
                    ['title' => 'snippet 3'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_filter_by_sources()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1',
                'source' => 'www.google.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2',
                'source' => 'www.bing.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-26 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'source' => 'twitter.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-27 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
                'sources' => ['bing','twitter'],
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 2,
                'data' => [
                    ['title' => 'snippet 2'],
                    ['title' => 'snippet 3'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_ignore_sources_filter_for_unknown_source()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1',
                'source' => 'www.google.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2',
                'source' => 'www.bing.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-26 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'source' => 'twitter.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-27 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
                'sources' => ['unknown'],
            ]))
            ->seeJsonSubset(['recordsTotal' => 3]);
    }

    /**
     * @test
     */
    public function should_be_able_to_filter_by_categories()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1',
                'source' => 'www.google.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2',
                'source' => 'www.bing.com',
                'category' => 'spam',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-26 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'source' => 'twitter.com',
                'category' => 'phising',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-27 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
                'categories' => ['spam','phising'],
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 2,
                'data' => [
                    ['title' => 'snippet 2'],
                    ['title' => 'snippet 3'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_ignore_categories_filter_for_unknown_category()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1',
                'source' => 'www.google.com',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2',
                'source' => 'www.bing.com',
                'category' => 'spam',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-26 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'source' => 'twitter.com',
                'category' => 'phising',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-27 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.search', ['foo',
                'from' => (new DateTimeImmutable('-3 day'))->format('Y-m-d'),
                'to' => (new DateTimeImmutable('-1 day'))->format('Y-m-d'),
                'categories' => ['unknown','phising'],
            ]))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [
                    ['title' => 'snippet 3'],
                ]
            ]);
    }
}
