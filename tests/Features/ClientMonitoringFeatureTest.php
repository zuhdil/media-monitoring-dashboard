<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;
use DateTimeImmutable;

class ClientMonitoringFeatureTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function open_client_monitoring_page()
    {
        factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring', 'foo'))
            ->assertResponseOk()
            ->seePageIs(route('monitoring', 'foo'));
    }

    /**
     * @test
     */
    public function open_client_monitoring_for_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring', 'foo'));
    }

    /**
     * @test
     */
    public function show_only_recent_snippets()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-10 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => '-20 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
            $fixture->make([
                'title' => '-30 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-30 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring', 'foo'))
            ->seeJsonSubset([
                'recordsTotal' => 2,
                'data' => [
                    ['title' => '-10 hour'],
                    ['title' => '-20 hour'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function show_only_snippets_owned_by_specific_client()
    {
        $client1 = factory(Client::class)->create(['username' => 'foo']);
        $client2 = factory(Client::class)->create(['username' => 'bar']);

        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'owned by foo',
                'owner' => $client1->id,
                'timestamp' => new DateTimeImmutable('now'),
            ]),
            $fixture->make([
                'title' => 'owned by bar',
                'owner' => $client2->id,
                'timestamp' => new DateTimeImmutable('now'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring', 'foo'))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [['title' => 'owned by foo']]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_order_by_date_asc()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-10 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => '-15 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-15 hour'),
            ]),
            $fixture->make([
                'title' => '-20 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring', ['foo', 'order' => 'asc']))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [
                    ['title' => '-20 hour'],
                    ['title' => '-15 hour'],
                    ['title' => '-10 hour'],
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_paginate_results()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-10 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => '-15 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-15 hour'),
            ]),
            $fixture->make([
                'title' => '-20 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring', ['foo', 'start'=> 1, 'length' => 1]))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [['title' => '-15 hour']]
            ]);
    }

    /**
     * @test
     */
    public function should_not_show_categorized_snippets()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-15 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring', 'foo'))
            ->seeJsonSubset([
                'recordsTotal' => 2,
                'data' => [
                    ['title' => 'snippet 1'],
                    ['title' => 'snippet 3']
                ]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_filter_by_source()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'snippet 1',
                'source' => 'twitter.com',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-10 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 2',
                'source' => 'www.google.com',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-15 hour'),
            ]),
            $fixture->make([
                'title' => 'snippet 3',
                'source' => 'www.bing.com',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-20 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring', ['foo', 'source' => 'google']))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [['title' => 'snippet 2']]
            ]);
    }
}
