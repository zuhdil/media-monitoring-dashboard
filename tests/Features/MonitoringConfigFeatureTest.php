<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Client;
use App\User;
use App\MonitoringConfig;

class MonitoringConfigFeatureTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function open_client_monitoring_config_page()
    {
        factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('clients.config', 'foo'))
            ->assertResponseOk()
            ->seePageIs(route('clients.config', 'foo'));
    }

    /**
     * @test
     */
    public function open_client_monitoring_config_for_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('clients.config', 'foo'));
    }

    /**
     * @test
     */
    public function create_new_monitoring_config()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->post(route('clients.config', 'foo'), [
                'keyword' => 'bar',
                'started_at' => '2016-01-01',
            ])
            ->seeInDatabase('monitoring_configs', [
                'client_id' => $client->id,
                'keyword' => 'bar',
                'started_at' => '2016-01-01 00:00:00',
            ]);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_monitoring_config_using_empty_data()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('clients.config', 'foo'), [
                'keyword' => '',
                'started_at' => '',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_monitoring_config_using_invalid_date()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('clients.config', 'foo'), [
                'keyword' => 'bar',
                'started_at' => 'invalid-date',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function get_client_monitoring_configs_data()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $configs = factory(MonitoringConfig::class, 3)->make();

        $client->monitoringConfigs()->saveMany($configs);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('clients.config', 'foo'))
            ->seeJsonStructure([
                'draw',
                'recordsTotal',
                'recordsFiltered',
                'data' => [
                    '*' => [
                        'id',
                        'keyword',
                        'started_at',
                        'created_at',
                        'links' => ['_self'],
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function delete_monitoring_config()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $config = factory(MonitoringConfig::class)->make();
        $client->monitoringConfigs()->save($config);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('delete', route('clients.config.delete', [$client->username, $config->id]))
            ->missingFromDatabase('monitoring_configs', ['keyword' => $config->keyword]);
    }
}
