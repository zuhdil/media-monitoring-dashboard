<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;
use DateTimeImmutable;

class ClientUncategorizedSnippetsFeatureTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function open_client_uncategorized_snippets_page()
    {
        factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring.uncategorized', 'foo'))
            ->assertResponseOk()
            ->seePageIs(route('monitoring.uncategorized', 'foo'));
    }

    /**
     * @test
     */
    public function open_client_uncategorized_snippets_for_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('monitoring.uncategorized', 'foo'));
    }

    /**
     * @test
     */
    public function only_show_past_snippets_with_no_category()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-1 day',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-1 day'),
            ]),
            $fixture->make([
                'title' => '-2 day',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-2 day'),
            ]),
            $fixture->make([
                'title' => '-3 day',
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-3 day'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.uncategorized', 'foo'))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [['title' => '-2 day']]
            ]);
    }

    /**
     * @test
     */
    public function show_only_snippets_owned_by_specific_client()
    {
        $client1 = factory(Client::class)->create(['username' => 'foo']);
        $client2 = factory(Client::class)->create(['username' => 'bar']);

        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => 'owned by foo',
                'owner' => $client1->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => 'owned by bar',
                'owner' => $client2->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ])
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.uncategorized', 'foo'))
            ->seeJsonSubset([
                'recordsTotal' => 1,
                'data' => [['title' => 'owned by foo']]
            ]);
    }

    /**
     * @test
     */
    public function should_be_able_to_order_by_date_asc()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-25 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => '-36 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-36 hour'),
            ]),
            $fixture->make([
                'title' => '-49 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-49 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.uncategorized', ['foo', 'order' => 'asc']))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [
                    ['title' => '-49 hour'],
                    ['title' => '-36 hour'],
                    ['title' => '-25 hour'],
                ]
            ]); 
    }

    /**
     * @test
     */
    public function should_be_able_to_paginate_results()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);
        $fixture = $this->getFixture();
        $fixture->getDAO()->bulkIndex([
            $fixture->make([
                'title' => '-25 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-25 hour'),
            ]),
            $fixture->make([
                'title' => '-36 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-36 hour'),
            ]),
            $fixture->make([
                'title' => '-49 hour',
                'owner' => $client->id,
                'timestamp' => new DateTimeImmutable('-49 hour'),
            ]),
        ], true);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('monitoring.uncategorized', ['foo', 'start' => 1, 'length' => 1]))
            ->seeJsonSubset([
                'recordsTotal' => 3,
                'data' => [['title' => '-36 hour']]
            ]);
    }
}
