<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Client;
use App\User;

class AuthenticationFeatureTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function unauthenticated_access_should_redirected_to_login_page()
    {
        $this->visit(route('home'))
            ->seePageIs(route('login'));
    }

    /**
     * @test
     */
    public function admin_login()
    {
        factory(User::class)->create([
            'username' => 'foo',
            'password' => bcrypt('bar'),
        ]);

        $this->visit(route('login'))
            ->type('foo', 'username')
            ->type('bar', 'password')
            ->press('Login')
            ->seePageIs(route('clients'))
            ->seeLink('Logout', route('logout'));
    }

    /**
     * @test
     */
    public function client_login()
    {
        $client = factory(Client::class)->create([
            'username' => 'foo',
            'password' => bcrypt('bar'),
        ]);

        $this->visit(route('login'))
            ->type('foo', 'username')
            ->type('bar', 'password')
            ->press('Login')
            ->seePageIs(route('monitoring', $client->username))
            ->seeLink('Logout', route('logout'));
    }

    /**
     * @test
     */
    public function should_bypass_login_page_if_authenticated()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('login'))
            ->seePageIs(route('clients'));
    }

    /**
     * @test
     */
    public function admin_should_see_site_navigation_menu()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('home'))
            ->seeLink('Clients', route('clients'));
    }

    /**
     * @test
     */
    public function client_should_not_see_site_navigation_menu()
    {
        $client = factory(Client::class)->create([
            'username' => 'foo',
            'password' => bcrypt('bar'),
        ]);

        $this->actingAs($client)
            ->visit(route('home'))
            ->dontSeeLink('Clients', route('clients'));
    }
}
