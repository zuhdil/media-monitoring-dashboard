<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;
use DateTimeImmutable as Date;

class ReportsFeaturesTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function open_daily_report_page()
    {
        factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('reports.daily', 'foo'))
            ->assertResponseOk()
            ->seePageIs(route('reports.daily', 'foo'));
    }

    /**
     * @test
     */
    public function open_daily_report_page_for_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('reports.daily', 'foo'));
    }

    /**
     * @test
     */
    public function open_monthly_report_page()
    {
        factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('reports.monthly', 'foo'))
            ->assertResponseOk()
            ->seePageIs(route('reports.monthly', 'foo'));
    }

    /**
     * @test
     */
    public function open_monthly_report_page_for_nonexisted_client_should_throw_exception()
    {
        $this->expectException(HttpException::class);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('reports.monthly', 'foo'));
    }

    /**
     * @test
     */
    public function open_monthly_report_page_should_render_view()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();
        $client = factory(Client::class)->create();

        $dao->bulkIndex([
            $fixture->make([
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-01 01:00:00'),
            ]),
            $fixture->make([
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-01 02:00:00'),
            ]),
            $fixture->make([
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-01 04:00:00'),
            ]),
        ], true);

        $response = $dao->dateHistogramForClient($client, new Date('2016-10-01 00:00:00'), new Date('2016-10-01 05:00:00'), 'hour');

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('reports.monthly', [$client->username, 'month' => '2016-10']))
            ->assertResponseOk();
    }
}
