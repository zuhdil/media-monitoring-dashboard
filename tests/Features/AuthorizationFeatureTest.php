<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\HttpException;
use App\Tests\WithSnippetsFixture;
use App\Client;
use App\User;

class AuthorizationFeatureTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function clients_page_should_not_be_accessible_by_client()
    {
        $client = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($client)
            ->visit(route('clients'));
    }

    /**
     * @test
     */
    public function posting_request_to_clients_page_should_not_be_accessible_by_client()
    {
        $client = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($client)
            ->makeRequest('post', route('clients'));
    }

    /**
     * @test
     */
    public function client_create_should_not_be_accessible_by_client()
    {
        $client = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($client)
            ->visit(route('clients.create'));
    }

    /**
     * @test
     */
    public function client_update_request_should_not_be_accessible_by_client()
    {
        $client = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($client)
            ->makeRequest('patch', route('clients.self', $client->username));
    }

    /**
     * @test
     */
    public function client_delete_request_should_not_be_accessible_by_client()
    {
        $client = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($client)
            ->makeRequest('delete', route('clients.self', $client->username));
    }

    /**
     * @test
     */
    public function client_config_page_should_not_be_accessible_by_client()
    {
        $client = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($client)
            ->visit(route('clients.config', $client->username));
    }

    /**
     * @test
     */
    public function client_config_update_request_should_not_be_accessible_by_client()
    {
        $client = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($client)
            ->makeRequest('post', route('clients.config', $client->username));
    }

    /**
     * @test
     * @dataProvider monitoringRoutesProvider
     */
    public function monitoring_page_should_be_accessible_by_client($routeName)
    {
        $client = factory(Client::class)->create();

        $this->actingAs($client)
            ->visit(route($routeName, $client->username))
            ->seePageIs(route($routeName, $client->username));
    }

    /**
     * @test
     * @dataProvider monitoringRoutesProvider
     */
    public function monitoring_page_should_not_be_accessible_by_other_client($routeName)
    {
        $client = factory(Client::class)->create();
        $other = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($other)
            ->visit(route($routeName, $client->username));
    }

    public function monitoringRoutesProvider()
    {
        return [
            ['monitoring'],
            ['monitoring.search'],
            ['monitoring.categorized'],
            ['monitoring.uncategorized'],
            ['reports.daily'],
        ];
    }

    /**
     * @test
     */
    public function update_snippet_should_be_accessible_by_client()
    {
        $dao = $this->getFixture()->getDAO();
        $snippet = $this->getFixture()->make();
        $dao->bulkIndex([$snippet], true);

        $client = factory(Client::class)->create();

        $this->actingAs($client)
            ->makeRequest(
                'post',
                route('snippets.update', [$client->username, $snippet['id']]),
                ['category' => 'safe-content']
            );
    }

    /**
     * @test
     */
    public function update_snippet_should_not_be_accessible_by_other_client()
    {
        $dao = $this->getFixture()->getDAO();
        $snippet = $this->getFixture()->make();
        $dao->bulkIndex([$snippet], true);

        $client = factory(Client::class)->create();
        $other = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($other)
            ->makeRequest(
                'post',
                route('snippets.update', [$client->username, $snippet['id']])
            );
    }

    /**
     * @test
     */
    public function bulk_update_snippet_should_be_accessible_by_client()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();

        $snippet1 = $fixture->make(['title' => 'snippet 1']);
        $snippet2 = $fixture->make(['title' => 'snippet 2']);
        $dao->bulkIndex([$snippet1, $snippet2], true);

        $client = factory(Client::class)->create();

        $this->actingAs($client)->makeRequest(
            'post',
            route('snippets.bulk_update', $client->username),
            ['category' => 'safe-content', 'snippets' => [$snippet1['id'], $snippet2['id']]]
        );
    }

    /**
     * @test
     */
    public function bulk_update_snippet_should_not_be_accessible_by_other_client()
    {
        $dao = $this->getFixture()->getDAO();
        $snippet = $this->getFixture()->make();
        $dao->bulkIndex([$snippet], true);

        $client = factory(Client::class)->create();
        $other = factory(Client::class)->create();

        $this->expectException(HttpException::class);
        $this->expectExceptionMessage('Received status code [403]');

        $this->actingAs($other)->makeRequest(
            'post',
            route('snippets.bulk_update', $client->username)
        );
    }
}
