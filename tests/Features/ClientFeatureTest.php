<?php
namespace App\Tests\Features;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Client;
use App\User;

class ClientFeatureTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function open_add_client_form_page()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('clients.create'))
            ->assertResponseOk()
            ->seePageIs(route('clients.create'));
    }

    /**
     * @test
     */
    public function create_new_client()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->post(route('clients'), [
                'name'=> 'john doe',
                'username'=> 'johndoe',
                'password'=> '123456',
                'password_confirmation'=> '123456',
            ])
            ->seeInDatabase('clients', [
                'name' => 'john doe',
                'username' => 'johndoe'
            ]);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_client_using_empty_data()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('clients'), [
                'name'=> ' ',
                'username'=> ' ',
                'password'=> ' ',
                'password_confirmation'=> ' ',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_client_using_invalid_password_confirmation()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('clients'), [
                'name'=> 'john doe',
                'username'=> 'johndoe',
                'password'=> '123456',
                'password_confirmation'=> '123455',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_client_using_existing_username()
    {
        factory(Client::class)->create(['username' => 'johndoe']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('clients'), [
                'name'=> 'john doe',
                'username'=> 'johndoe',
                'password'=> '123456',
                'password_confirmation'=> '123456',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function should_failed_to_create_new_client_using_exiting_user_username()
    {
        factory(User::class)->create(['username' => 'johndoe']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('post', route('clients'), [
                'name'=> 'john doe',
                'username'=> 'johndoe',
                'password'=> '123456',
                'password_confirmation'=> '123456',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function open_client_list_page()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit(route('clients'))
            ->assertResponseOk()
            ->seePageIs(route('clients'));
    }

    /**
     * @test
     */
    public function get_client_list_data()
    {
        factory(Client::class, 3)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('get', route('clients'))
            ->seeJsonStructure([
                'draw',
                'recordsTotal',
                'recordsFiltered',
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'username',
                        'created_at',
                        'links' => [
                            'monitoring',
                            'config',
                        ],
                    ]
                ]
            ]);
    }

    /**
     * @test
     */
    public function delete_client()
    {
        $client = factory(Client::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('delete', route('clients.self', $client->username))
            ->missingFromDatabase('clients', ['username' => $client->username]);
    }

    /**
     * @test
     */
    public function update_client_should_failed_given_empty_data()
    {
        $client = factory(Client::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'name'=> ' ',
                'username'=> ' ',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function update_client_username_should_failed_given_username_already_used_by_other_client()
    {
        factory(Client::class)->create(['username' => 'foo']);
        $client = factory(Client::class)->create(['username' => 'bar']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'name'=> 'update',
                'username'=> 'foo',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function update_client_username_should_failed_given_username_already_used_by_user()
    {
        factory(User::class)->create(['username' => 'foo']);
        $client = factory(Client::class)->create(['username' => 'bar']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'name'=> 'update',
                'username'=> 'foo',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function update_client_should_update_database_given_valid_input()
    {
        $client = factory(Client::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'name'=> 'john doe',
                'username'=> 'johndoe',
            ])
            ->seeInDatabase('clients', [
                'name' => 'john doe',
                'username' => 'johndoe'
            ]);
    }

    /**
     * @test
     */
    public function reset_password_should_failed_given_empty_values()
    {
        $client = factory(Client::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'password'=> ' ',
                'password_confirmation'=> ' ',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function reset_password_should_failed_given_invalid_confirmation()
    {
        $client = factory(Client::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'password'=> 'opensesame',
                'password_confirmation'=> 'abracadabra',
            ])
            ->assertResponseStatus(422);
    }

    /**
     * @test
     */
    public function reset_password_should_update_client_password()
    {
        $client = factory(Client::class)->create();

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'password'=> 'opensesame',
                'password_confirmation'=> 'opensesame',
            ]);

        $updated = Client::find($client->id);
        $this->assertTrue($this->app['hash']->check('opensesame', $updated->password));
    }

    /**
     * @test
     */
    public function should_ignore_username_if_username_input_not_changed()
    {
        $client = factory(Client::class)->create(['username' => 'foo']);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('patch', route('clients.self', $client->username), [
                'name'=> 'john doe',
                'username'=> 'foo',
            ])
            ->seeInDatabase('clients', [
                'name' => 'john doe',
                'username' => 'foo'
            ]);
    }

    /**
     * @test
     */
    public function deactivate_client()
    {
        $client = factory(Client::class)->create(['is_active' => true]);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('delete', route('clients.active', $client->username))
            ->seeInDatabase('clients', [
                'username' => $client->username,
                'is_active' => false,
            ]);
    }

    /**
     * @test
     */
    public function activate_client()
    {
        $client = factory(Client::class)->create(['is_active' => false]);

        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->json('put', route('clients.active', $client->username))
            ->seeInDatabase('clients', [
                'username' => $client->username,
                'is_active' => true,
            ]);
    }
}
