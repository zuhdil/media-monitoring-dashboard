<?php
namespace App\Tests;

use Illuminate\Foundation\Application;
use Faker\Generator as Faker;
use Elasticsearch\Client as Elasticsearch;
use App\SnippetsDAO;
use DateTimeInterface;

class SnippetsDAOTestFixture
{
    private $app;
    private $index;

    private $client;
    private $dao;
    private $faker;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->index = $this->app['config']['scraper.elasticsearch_index'];
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getDAO()
    {
        return $this->app->make(SnippetsDAO::class);
    }

    public function getClient()
    {
        return $this->app->make(Elasticsearch::class);
    }

    public function getFaker()
    {
        return $this->app->make(Faker::class);
    }

    public function make(array $attrs = [])
    {
        $faker = $this->getFaker();

        $dummy = [
            'id' => $faker->uuid,
            'owner' => $faker->uuid,
            'source' => $faker->randomElement([
                'www.google.com',
                'www.bing.com',
                'twitter.com',
            ]),
            'title' => $faker->sentence(),
            'snippet' => $faker->paragraph(),
            'url' => $faker->url,
            'timestamp' => $faker->iso8601(),
        ];

        $snippet = array_replace($dummy, $attrs);

        if ($snippet['timestamp'] instanceof DateTimeInterface) {
            $snippet['timestamp'] = $snippet['timestamp']->format(DATE_ISO8601);
        }

        return $snippet;
    }

    public function makeMany($count, array $attrs = [])
    {
        $snippets = [];

        for ($i = 0; $i < $count; $i++) {
            $snippets[] = $this->make($attrs);
        }

        return $snippets;
    }
}
