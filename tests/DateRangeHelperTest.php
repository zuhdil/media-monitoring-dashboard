<?php
namespace App\Tests;

use PHPUnit_Framework_TestCase as TestCase;
use DateTimeInterface;
use App\DateRangeHelper;

class DateRangeHelperTest extends TestCase
{
    /**
     * @test
     */
    public function getStartedAt_return_DateTimeInterface_object()
    {
        $daterange = new DateRangeHelper('2016-01-01', '2016-01-01');
        $this->assertInstanceOf(DateTimeInterface::class, $daterange->getStartedAt());
    }

    /**
     * @test
     */
    public function getStartedAt_return_midnight_of_the_start_date()
    {
        $daterange = new DateRangeHelper('2016-01-01', '2016-01-01');
        $this->assertEquals('2016-01-01 00:00:00', $daterange->getStartedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function getStartedAt_always_before_end_date()
    {
        $daterange = new DateRangeHelper('2016-01-04', '2016-01-03', '2016-01-02');
        $this->assertEquals('2016-01-01 00:00:00', $daterange->getStartedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function getStartedAt_always_before_end_date2()
    {
        $daterange = new DateRangeHelper('2016-01-03', '2016-01-02');
        $this->assertEquals('2016-01-02 00:00:00', $daterange->getStartedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function getStartedAt_always_before_end_date3()
    {
        $daterange = new DateRangeHelper('2016-01-03', '2016-01-02', '2016-01-02 15:00:00');
        $this->assertEquals('2016-01-02 00:00:00', $daterange->getStartedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function getStartedAt_with_month_as_range_return_day_of_the_previous_month_at_midnight()
    {
        $daterange = new DateRangeHelper('2016-02-01', '2016-02-28', '2016-02-01', 'month');
        $this->assertEquals('2016-01-01 00:00:00', $daterange->getStartedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function getEndedAt_return_DateTimeInterface_object()
    {
        $daterange = new DateRangeHelper('2016-01-01', '2016-01-01');
        $this->assertInstanceOf(DateTimeInterface::class, $daterange->getEndedAt());
    }

    /**
     * @test
     */
    public function getEndedAt_return_midnight_of_next_day_after_end_date()
    {
        $daterange = new DateRangeHelper('2016-01-01', '2016-01-01');
        $this->assertEquals('2016-01-02 00:00:00', $daterange->getEndedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function getEndedAt_when_max_date_less_than_end_date_should_return_max_date()
    {
        $daterange = new DateRangeHelper('2016-01-01', '2016-01-01', '2016-01-01 12:00:00');
        $this->assertEquals('2016-01-01 12:00:00', $daterange->getEndedAt()->format('Y-m-d H:i:s'));
    }

    /**
     * @test
     */
    public function getEndedAt_when_max_date_greater_than_end_date_should_return_end_date()
    {
        $daterange = new DateRangeHelper('2016-01-01', '2016-01-01', '2016-01-02 12:00:00');
        $this->assertEquals('2016-01-02 00:00:00', $daterange->getEndedAt()->format('Y-m-d H:i:s'));
    }
}
