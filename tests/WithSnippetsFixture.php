<?php
namespace App\Tests;

trait WithSnippetsFixture
{
    private $fixture;
    private $index;

    protected function getFixture()
    {
        if (null === $this->fixture) {
            $this->fixture = new SnippetsDAOTestFixture($this->app);
        }

        return $this->fixture;
    }

    protected function getIndex()
    {
        return $this->getFixture()->getIndex();
    }

    /**
     * @before
     */
    public function prepareElasticsearch()
    {
        $dao = $this->getFixture()->getDAO();

        $dao->install();

        $this->beforeApplicationDestroyed(function () use ($dao) {
            $dao->uninstall();
        });
    }
}
