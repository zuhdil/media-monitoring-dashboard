<?php
namespace App\Tests;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Contracts\Hashing\Hasher;
use App\CustomUserProvider;
use App\User;
use App\Client;

class CustomUserProviderTest extends TestCase
{
    use DatabaseTransactions;

    private function makeSUT()
    {
        return new CustomUserProvider($this->app->make(Hasher::class));
    }

    /**
     * @test
     */
    public function given_user_not_exists_when_retrieve_by_id_should_return_null()
    {
        $provider = $this->makeSUT();

        $user = $provider->retrieveById(1);

        $this->assertNull($user);
    }

    /**
     * @test
     */
    public function given_user_exists_when_retrieve_by_id_should_return_user_object()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveById($user->id);

        $this->assertInstanceOf(User::class, $result);
    }

    /**
     * @test
     */
    public function given_client_exists_when_retrieve_by_id_should_return_client_object()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveById($client->id);

        $this->assertInstanceOf(Client::class, $result);
    }

    /**
     * @test
     */
    public function given_invalid_token_when_retrieve_user_by_token_should_return_null()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveByToken($user->id, str_random(10));

        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function given_invalid_token_when_retrieve_client_by_token_should_return_null()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveByToken($client->id, str_random(10));

        $this->assertNull($result);
    }

    /**
     * @test
     */
    public function given_valid_token_when_retrieve_user_by_token_should_return_user_object()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveByToken($user->id, $user->remember_token);

        $this->assertEquals($user->name, $result->name);
    }

    /**
     * @test
     */
    public function given_valid_token_when_retrieve_client_by_token_should_return_client_object()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveByToken($client->id, $client->remember_token);

        $this->assertEquals($client->name, $result->name);
    }

    /**
     * @test
     */
    public function when_update_user_remember_token_should_update_user_remember_token_attribute()
    {
        $user = factory(User::class)->create();
        $provider = $this->makeSUT();
        $newToken = str_random(10);

        $provider->updateRememberToken($user, $newToken);

        $this->assertEquals($newToken, User::find($user->id)->remember_token);
    }

    /**
     * @test
     */
    public function when_update_client_remember_token_should_update_client_remember_token_attribute()
    {
        $client = factory(Client::class)->create();
        $provider = $this->makeSUT();
        $newToken = str_random(10);

        $provider->updateRememberToken($client, $newToken);

        $this->assertEquals($newToken, Client::find($client->id)->remember_token);
    }

    /**
     * @test
     */
    public function given_user_not_exists_when_retrieve_by_credentials_should_return_null()
    {
        $provider = $this->makeSUT();

        $user = $provider->retrieveByCredentials(['username' => 'foo']);

        $this->assertNull($user);
    }

    /**
     * @test
     */
    public function given_user_exists_when_retrieve_by_credentials_should_return_user_object()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveByCredentials(['username' => $user->username]);

        $this->assertInstanceOf(User::class, $result);
    }

    /**
     * @test
     */
    public function given_client_exists_when_retrieve_by_credentials_should_return_client_object()
    {
        $user = factory(User::class)->create();
        $client = factory(Client::class)->create();

        $provider = $this->makeSUT();

        $result = $provider->retrieveByCredentials(['username' => $client->username]);

        $this->assertInstanceOf(Client::class, $result);
    }

    /**
     * @test
     */
    public function given_invalid_password_when_validate_user_credentials_should_return_false()
    {
        $user = factory(User::class)->create();
        $provider = $this->makeSUT();
        $result = $provider->retrieveByCredentials(['username' => $user->username]);

        $this->assertFalse(
            $provider->validateCredentials($result, ['password' => 'invalid'])
        );
    }

    /**
     * @test
     */
    public function given_valid_password_when_validate_user_credentials_should_return_true()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt('secret')
        ]);

        $provider = $this->makeSUT();
        $result = $provider->retrieveByCredentials(['username' => $user->username]);

        $this->assertTrue(
            $provider->validateCredentials($result, ['password' => 'secret'])
        );
    }

    /**
     * @test
     */
    public function given_invalid_password_when_validate_client_credentials_should_return_false()
    {
        $client = factory(Client::class)->create();
        $provider = $this->makeSUT();
        $result = $provider->retrieveByCredentials(['username' => $client->username]);

        $this->assertFalse(
            $provider->validateCredentials($result, ['password' => 'invalid'])
        );
    }

    /**
     * @test
     */
    public function given_valid_password_when_validate_client_credentials_should_return_true()
    {
        $client = factory(Client::class)->create([
            'password' => bcrypt('secret')
        ]);

        $provider = $this->makeSUT();
        $result = $provider->retrieveByCredentials(['username' => $client->username]);

        $this->assertTrue(
            $provider->validateCredentials($result, ['password' => 'secret'])
        );
    }
}
