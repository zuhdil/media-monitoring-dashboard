<?php
namespace App\Tests;

use TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Client;
use DateTimeImmutable as Date;

class SnippetsDAOTest extends TestCase
{
    use DatabaseTransactions, WithSnippetsFixture;

    /**
     * @test
     */
    public function install_and_uninstall()
    {
        $client = $this->getFixture()->getClient();
        $dao = $this->getFixture()->getDAO();

        $dao->uninstall();
        $this->assertFalse($client->indices()->exists(['index' => $this->getIndex()]), 'should delete index');

        $dao->install();
        $this->assertTrue($client->indices()->exists(['index' => $this->getIndex()]), 'should create index');
    }

    /**
     * test
     */
    public function index_and_retrieve()
    {
        $snippet = $this->getFixture()->make(['title' => 'foo']);

        $this->getFixture()->getDAO()->bulkIndex([$snippet]);

        $result = $this->getFixture()->getDAO()->get($snippet['id']);

        $this->assertEquals($snippet['title'], $result['title']);
    }

    /**
     * @test
     */
    public function bulkIndex_should_index_all_snippets()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();

        $snippets = [
            $fixture->make(),
            $fixture->make(),
            $fixture->make(),
        ];

        $dao->bulkIndex($snippets, true);

        $result = $dao->search(['query' => ['match_all' => ['boost' => 1]]]);

        $this->assertEquals($result['total'], count($snippets));
    }

    /**
     * @test
     */
    public function update_index()
    {
        $dao = $this->getFixture()->getDAO();
        $snippet = $this->getFixture()->make(['title' => 'foo']);
        $dao->bulkIndex([$snippet]);

        $dao->update($snippet['id'], ['title' => 'bar']);

        $result = $dao->get($snippet['id']);

        $this->assertEquals('bar', $result['title']);
    }

    /**
     * @test
     */
    public function bulkUpdate_should_update_all_snippets()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();

        $snippet1 = $fixture->make(['title' => 'snippet 1']);
        $snippet2 = $fixture->make(['title' => 'snippet 2']);

        $dao->bulkIndex([$snippet1, $snippet2], true);

        $dao->bulkUpdate([
            $snippet1['id'] => ['title' => 'snippet 1 updated'],
            $snippet2['id'] => ['title' => 'snippet 2 updated'],
        ], true);

        $result1 = $dao->get($snippet1['id']);
        $result2 = $dao->get($snippet2['id']);

        $this->assertEquals('snippet 1 updated', $result1['title']);
        $this->assertEquals('snippet 2 updated', $result2['title']);
    }

    /**
     * @test
     */
    public function summarizeCategoryForClient_should_return_snippets_summary_by_category()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();
        $client = factory(Client::class)->create();
        $dao->bulkIndex([
            $fixture->make([
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new Date('2016-10-10 01:00:00'),
            ]),
            $fixture->make([
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new Date('2016-10-10 02:00:00'),
            ]),
            $fixture->make([
                'category' => 'spam',
                'owner' => $client->id,
                'timestamp' => new Date('2016-10-10 03:00:00'),
            ]),
        ], true);

        $response = $dao->summarizeCategoryForClient($client, new Date('2016-10-10 00:00:00'), new Date('2016-10-11 00:00:00'));

        $this->assertArraySubset([
            'count' => 3,
            'by_category' => [
                'safe-content' => ['count' => 2],
                'spam' => ['count' => 1]
            ]
        ], $response);
    }

    /**
     * @test
     */
    public function summarizeCategoryForClient_should_summarize_only_for_requested_date()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();
        $client = factory(Client::class)->create();
        $dao->bulkIndex([
            $fixture->make([
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new Date('2016-10-11 01:00:00'),
            ]),
            $fixture->make([
                'category' => 'safe-content',
                'owner' => $client->id,
                'timestamp' => new Date('2016-10-10 02:00:00'),
            ]),
            $fixture->make([
                'category' => 'spam',
                'owner' => $client->id,
                'timestamp' => new Date('2016-10-10 03:00:00'),
            ]),
        ], true);

        $response = $dao->summarizeCategoryForClient($client, new Date('2016-10-10 00:00:00'), new Date('2016-10-11 00:00:00'));

        $this->assertArraySubset([
            'count' => 2,
            'by_category' => [
                'safe-content' => ['count' => 1],
                'spam' => ['count' => 1]
            ]
        ], $response);
    }

    /**
     * @test
     */
    public function summarizeCategoryForClient_should_summarize_sources_under_category()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();
        $client = factory(Client::class)->create();

        $dao->bulkIndex(array_merge(
            $fixture->makeMany(2, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-01 01:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'spam',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-01 02:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.bing.com',
                'timestamp' => new Date('2016-10-01 02:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-02 01:00:00'),
            ]),
            $fixture->makeMany(2, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-02 02:00:00'),
            ]),
            $fixture->makeMany(2, [
                'owner' => $client->id,
                'category' => 'phising',
                'source' => 'www.bing.com',
                'timestamp' => new Date('2016-10-02 02:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'spam',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-04 01:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-04 02:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'phising',
                'source' => 'www.bing.com',
                'timestamp' => new Date('2016-10-04 02:00:00'),
            ])
        ), true);

        $response = $dao->summarizeCategoryForClient($client, new Date('2016-10-01 00:00:00'), new Date('2016-10-05 00:00:00'));

        $this->assertArraySubset([
            'count' => 12,
            'by_category' => [
                'safe-content' => [
                    'count' => 7,
                    'by_source' => [
                        'www.google.com' => ['count' => 3],
                        'twitter.com' => ['count' => 3],
                        'www.bing.com' => ['count' => 1],
                    ],
                ],
                'spam' => [
                    'count' => 2,
                    'by_source' => [
                        'www.google.com' => ['count' => 1],
                        'twitter.com' => ['count' => 1],
                    ],
                ],
                'phising' => [
                    'count' => 3,
                    'by_source' => [
                        'www.bing.com' => ['count' => 3],
                    ],
                ],
            ],
        ], $response);
    }

    /**
     * @test
     */
    public function dateHistogramForClient_daily_interval_between_datetimes()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();
        $client = factory(Client::class)->create();

        $dao->bulkIndex(array_merge(
            // 2016-10-01
            $fixture->makeMany(2, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-01 01:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'spam',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-01 02:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.bing.com',
                'timestamp' => new Date('2016-10-01 02:00:00'),
            ]),
            // 2016-10-02
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-02 01:00:00'),
            ]),
            $fixture->makeMany(2, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-02 02:00:00'),
            ]),
            $fixture->makeMany(2, [
                'owner' => $client->id,
                'category' => 'phising',
                'source' => 'www.bing.com',
                'timestamp' => new Date('2016-10-02 02:00:00'),
            ]),
            // 2016-10-03
            // 2016-10-04
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'spam',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-04 01:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-04 02:00:00'),
            ]),
            $fixture->makeMany(1, [
                'owner' => $client->id,
                'category' => 'phising',
                'source' => 'www.bing.com',
                'timestamp' => new Date('2016-10-04 02:00:00'),
            ])
        ), true);

        $response = $dao->dateHistogramForClient($client, new Date('2016-10-01 00:00:00'), new Date('2016-10-05 00:00:00'));

        $this->assertArraySubset([
            'count' => 12,
            'by_timestamp' => [
                '2016-10-01T00:00:00.000+07:00' => [
                    'count' => 4,
                    'by_category' => [
                        'safe-content' => ['count' => 3],
                        'spam' => ['count' => 1],
                    ],
                ],
                '2016-10-02T00:00:00.000+07:00' => [
                    'count' => 5,
                    'by_category' => [
                        'safe-content' => ['count' => 3],
                        'phising' => ['count' => 2],
                    ],
                ],
                '2016-10-03T00:00:00.000+07:00' => [
                    'count' => 0,
                    'by_category' => [],
                ],
                '2016-10-04T00:00:00.000+07:00' => [
                    'count' => 3,
                    'by_category' => [
                        'safe-content' => ['count' => 1],
                        'spam' => ['count' => 1],
                        'phising' => ['count' => 1],
                    ],
                ],
            ],
            'by_source' => [
                'www.google.com' => [
                    'count' => 4,
                    'by_timestamp' => [
                        '2016-10-01T00:00:00.000+07:00' => [
                            'count' => 2,
                            'by_category' => [
                                'safe-content' => ['count' => 2],
                            ],
                        ],
                        '2016-10-02T00:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'safe-content' => ['count' => 1],
                            ],
                        ],
                        '2016-10-03T00:00:00.000+07:00' => [
                            'count' => 0,
                            'by_category' => [],
                        ],
                        '2016-10-04T00:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'spam' => ['count' => 1],
                            ],
                        ],
                    ]
                ],
                'twitter.com' => [
                    'count' => 4,
                    'by_timestamp' => [
                        '2016-10-01T00:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'spam' => ['count' => 1],
                            ],
                        ],
                        '2016-10-02T00:00:00.000+07:00' => [
                            'count' => 2,
                            'by_category' => [
                                'safe-content' => ['count' => 2],
                            ],
                        ],
                        '2016-10-03T00:00:00.000+07:00' => [
                            'count' => 0,
                            'by_category' => [],
                        ],
                        '2016-10-04T00:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'safe-content' => ['count' => 1],
                            ],
                        ],
                    ]
                ],
                'www.bing.com' => [
                    'count' => 4,
                    'by_timestamp' => [
                        '2016-10-01T00:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'safe-content' => ['count' => 1],
                            ],
                        ],
                        '2016-10-02T00:00:00.000+07:00' => [
                            'count' => 2,
                            'by_category' => [
                                'phising' => ['count' => 2],
                            ],
                        ],
                        '2016-10-03T00:00:00.000+07:00' => [
                            'count' => 0,
                            'by_category' => [],
                        ],
                        '2016-10-04T00:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'phising' => ['count' => 1],
                            ],
                        ],
                    ]
                ],
            ]
        ], $response);
    }

    /**
     * @test
     */
    public function dateHistogramForClient_hourly_interval_between_datetimes()
    {
        $fixture = $this->getFixture();
        $dao = $fixture->getDAO();
        $client = factory(Client::class)->create();

        $dao->bulkIndex([
            $fixture->make([
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-01 01:00:00'),
            ]),
            $fixture->make([
                'owner' => $client->id,
                'category' => 'spam',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-01 01:10:00'),
            ]),
            $fixture->make([
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'twitter.com',
                'timestamp' => new Date('2016-10-01 02:00:00'),
            ]),
            $fixture->make([
                'owner' => $client->id,
                'category' => 'safe-content',
                'source' => 'www.google.com',
                'timestamp' => new Date('2016-10-01 04:00:00'),
            ]),
        ], true);

        $response = $dao->dateHistogramForClient($client, new Date('2016-10-01 00:00:00'), new Date('2016-10-01 05:00:00'), 'hour');

        $this->assertArraySubset([
            'count' => 4,
            'by_timestamp' => [
                '2016-10-01T01:00:00.000+07:00' => [
                    'count' => 2,
                    'by_category' => [
                        'safe-content' => ['count' => 1],
                        'spam' => ['count' => 1],
                    ],
                ],
                '2016-10-01T02:00:00.000+07:00' => [
                    'count' => 1,
                    'by_category' => [
                        'safe-content' => ['count' => 1],
                    ],
                ],
                '2016-10-01T03:00:00.000+07:00' => [
                    'count' => 0,
                    'by_category' => [],
                ],
                '2016-10-01T04:00:00.000+07:00' => [
                    'count' => 1,
                    'by_category' => [
                        'safe-content' => ['count' => 1],
                    ],
                ],
            ],
            'by_source' => [
                'www.google.com' => [
                    'count' => 2,
                    'by_timestamp' => [
                        '2016-10-01T01:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'safe-content' => ['count' => 1],
                            ],
                        ],
                        '2016-10-01T02:00:00.000+07:00' => [
                            'count' => 0,
                            'by_category' => [],
                        ],
                        '2016-10-01T03:00:00.000+07:00' => [
                            'count' => 0,
                            'by_category' => [],
                        ],
                        '2016-10-01T04:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'safe-content' => ['count' => 1],
                            ],
                        ],
                    ],
                ],
                'twitter.com' => [
                    'count' => 2,
                    'by_timestamp' => [
                        '2016-10-01T01:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'spam' => ['count' => 1],
                            ],
                        ],
                        '2016-10-01T02:00:00.000+07:00' => [
                            'count' => 1,
                            'by_category' => [
                                'safe-content' => ['count' => 1],
                            ],
                        ],
                    ],
                ],
            ],
        ], $response);
    }
}
