<?php
namespace App;

use DateTimeImmutable;

class DateRangeHelper
{
    private $start;
    private $end;

    public function __construct($start, $end, $max = null, $interval = 'day')
    {
        if (null !== $max && ! $max instanceof DateTimeImmutable) {
            $max = new DateTimeImmutable($max);
        }
        if (! $end instanceof DateTimeImmutable) {
            $end = new DateTimeImmutable($end);
        }

        $this->end = $this->determineEndedAt($end, $max);

        if (! $start instanceof DateTimeImmutable) {
            $start = new DateTimeImmutable($start);
        }

        $this->start = $this->determineStartedAt($start, $this->end, $interval)
            ->modify('midnight');
    }

    private function determineEndedAt(DateTimeImmutable $end, $max = null)
    {
        $endedAt = $end->modify('midnight next day');

        return (null !== $max && $endedAt > $max) ? $max : $endedAt;
    }

    private function determineStartedAt(DateTimeImmutable $start, DateTimeImmutable $end, $interval)
    {
        if ('00:00:00' == $end->format('H:i:s')) {
            $end = $end->modify('-1 '.$interval);
        }

        return ($start > $end) ? $end : $start;
    }

    public function getStartedAt()
    {
        return $this->start;
    }

    public function getEndedAt()
    {
        return $this->end;
    }
}
