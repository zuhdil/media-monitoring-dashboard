<?php
namespace App;

use DateTimeInterface;

class ClientSnippetsQuery
{
    private $dao;
    private $client;
    private $sources;
    private $categories;

    private $query;
    private $source;
    private $sourceIn;
    private $categoryIn;

    private $startedAt;
    private $endedAt;
    private $categorized = false;
    private $sort = 'desc';
    private $skip = 0;
    private $take = 10;

    public function __construct(SnippetsDAO $dao, Client $client, array $sources, array $categories)
    {
        $this->dao = $dao;
        $this->client = $client;
        $this->sources = $sources;
        $this->categories = $categories;
    }

    public function match($query)
    {
        $this->query = $query;
        return $this;
    }

    public function source($source)
    {
        $this->source = $source;
        return $this;
    }

    public function sourceIn(array $sources)
    {
        $this->sourceIn = $sources;
        return $this;
    }

    public function categoryIn(array $categories)
    {
        $this->categoryIn = $categories;
        return $this;
    }

    public function startedAt(DateTimeInterface $date)
    {
        $this->startedAt = $date;
        return $this;
    }

    public function endedAt(DateTimeInterface $date)
    {
        $this->endedAt = $date;
        return $this;
    }

    public function categorized($flag = true)
    {
        $this->categorized = $flag;
        return $this;
    }

    public function sort($sort)
    {
        $this->sort = in_array($sort, ['asc', 'desc']) ? $sort : 'desc';
        return $this;
    }

    public function skip($skip)
    {
        $this->skip = $skip ?: 0;
        return $this;
    }

    public function take($take)
    {
        $this->take = $take ?: 10;
        return $this;
    }

    public function get()
    {
        $params = [
            'query' => [
                'bool' => [
                    'filter' => [
                        'bool' => [
                            'must' => [['term' => ['owner' => $this->client->id]]],
                        ]
                    ]
                ]
            ],
            'sort' => ['timestamp' => ['order' => $this->sort]],
            'from' => $this->skip,
            'size' => $this->take,
        ];

        $haveCategory = ['exists' => ['field' => 'category']];
        if ($this->categorized) {
            $params['query']['bool']['filter']['bool']['must'][] = $haveCategory;
        }
        else {
            $params['query']['bool']['filter']['bool']['must_not'] = [$haveCategory];
        }

        $timestamp = [];
        if ($this->startedAt instanceof DateTimeInterface) {
            $timestamp['gte'] = $this->startedAt->format(DATE_ISO8601);
        }
        if ($this->endedAt instanceof DateTimeInterface) {
            $timestamp['lte'] = $this->endedAt->format(DATE_ISO8601);
        }
        $params['query']['bool']['filter']['bool']['must'][] = [
            'range' => ['timestamp' => $timestamp]
        ];

        if (! empty($this->query)) {
            $params['query']['bool']['must'] = [
                'multi_match' => [
                    'query' => $this->query,
                    'fields' => ['title', 'snippet', 'title.ind', 'snippet.ind'],
                ]
            ];
        }

        if (! empty($this->source) && array_key_exists($this->source, $this->sources)) {
            $params['query']['bool']['filter']['bool']['must'][] = [
                'term' => ['source' => $this->sources[$this->source]['value']]
            ];
        }

        if (! empty($this->sourceIn)) {
            $availableSources = $this->sources;
            $appliedSourcesFilter = array_map(function ($source) use ($availableSources) {
                return $availableSources[$source]['value'];
            }, array_intersect($this->sourceIn, array_keys($availableSources)));

            if (! empty($appliedSourcesFilter)) {
                $params['query']['bool']['filter']['bool']['must'][] = [
                    'terms' => ['source' => $appliedSourcesFilter]
                ];
            }
        }

        if (! empty($this->categoryIn)) {
            $appliedCategoriesFilter = array_values(array_intersect($this->categoryIn, array_keys($this->categories)));

            if (! empty($appliedCategoriesFilter)) {
                $params['query']['bool']['filter']['bool']['must'][] = [
                    'terms' => ['category' => $appliedCategoriesFilter]
                ];
            }
        }

        return $this->dao->search($params);
    }
}
