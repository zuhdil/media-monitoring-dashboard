<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class MonitoringConfig extends Model
{
    use WithUuid;

    public $incrementing = false;

    protected $fillable = ['keyword', 'started_at'];

    protected $dates = ['started_at', 'created_at', 'updated_at'];
}
