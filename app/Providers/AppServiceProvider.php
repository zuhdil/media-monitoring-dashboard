<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Elasticsearch\Client as Elasticsearch;
use Elasticsearch\ClientBuilder;
use App\SnippetsDAO;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Elasticsearch::class, function ($app) {
            $hosts = $app['config']['elasticsearch.hosts'];
            $logPath = $app['config']['elasticsearch.logPath'];
            $logger = ClientBuilder::defaultLogger($logPath);

            return ClientBuilder::create()
                ->setHosts($hosts)
                ->setLogger($logger)
                ->build();
        });

        $this->app->singleton(SnippetsDAO::class, function ($app) {
            return new SnippetsDAO(
                $app[Elasticsearch::class],
                $app['config']['scraper.sources'],
                $app['config']['scraper.categories'],
                $app['config']['scraper.elasticsearch_index']
            );
        });
    }
}
