<?php

namespace App\Providers;

use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\CustomUserProvider;
use App\User;
use App\Client;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        app('auth')->provider('custom', function($app) {
            return new CustomUserProvider($app->make(Hasher::class));
        });

        Gate::define('administer', function ($user) {
            return $user instanceof User;
        });

        Gate::define('monitoring', function ($user, $client) {
            return $user instanceof User || ($user instanceof Client && $user->id === $client->id);
        });
    }
}
