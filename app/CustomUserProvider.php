<?php
namespace App;

use Illuminate\Support\Str;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;

class CustomUserProvider implements UserProvider
{
    private $hasher;

    public function __construct(Hasher $hasher)
    {
        $this->hasher = $hasher;
    }

    public function retrieveById($identifier)
    {
        $client = Client::find($identifier);

        if ($client) {
            return $client;
        }

        return User::find($identifier);
    }

    public function retrieveByToken($identifier, $token)
    {
        $client = Client::where('id', $identifier)
            ->where('remember_token', $token)
            ->first();

        if ($client) {
            return $client;
        }

        return User::where('id', $identifier)
            ->where('remember_token', $token)
            ->first();
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setRememberToken($token);
        $user->save();
    }

    public function retrieveByCredentials(array $credentials)
    {
        $client = $this->getFirstWhere(
            (new Client())->newQuery(),
            $credentials
        );

        if ($client) {
            return $client;
        }

        return $this->getFirstWhere(
            (new User())->newQuery(),
            $credentials
        );
    }

    private function getFirstWhere(QueryBuilder $query, array $conditions)
    {
        foreach ($conditions as $key => $value) {
            if (!Str::contains($key, 'password')) {
                $query->where($key, $value);
            }
        }

        return $query->first();
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->hasher->check($credentials['password'], $user->getAuthPassword());
    }
}
