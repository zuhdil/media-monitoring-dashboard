<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\SnippetsDAO;
use DateTimeImmutable;
use App\DateRangeHelper;

class MonitoringController extends Controller
{
    private $snippets;
    private $categories;

    public function __construct(SnippetsDAO $snippets)
    {
        $this->snippets = $snippets;
    }

    public function index(Client $client, Request $request)
    {
        if ($request->wantsJson()) {
            $response = $this->snippets->belongsToClient($client)
                ->source($request->input('source'))
                ->startedAt(new DateTimeImmutable('-1 day'))
                ->sort($request->input('order'))
                ->take($request->input('length'))
                ->skip($request->input('start'))
                ->get();

            return $this->makeSearchResultResponse($client, $request, $response);
        }

        return view('monitoring.index', [
            'title' => $client->name . ' Monitoring',
            'disableSourceFilter' => false,
            'client' => $client,
            'dataUrl' => route('monitoring', $client->username),
            'queryParams' => [],
        ]);
    }

    public function categorized(Client $client, Request $request)
    {
        if ($request->wantsJson()) {
            $response = $this->snippets->belongsToClient($client)
                ->source($request->input('source'))
                ->categorized()
                ->startedAt(new DateTimeImmutable('-1 day'))
                ->sort($request->input('order'))
                ->take($request->input('length'))
                ->skip($request->input('start'))
                ->get();

            return $this->makeSearchResultResponse($client, $request, $response);
        }

        return view('monitoring.index', [
            'title' => $client->name . ' Monitoring, Categorized',
            'disableSourceFilter' => false,
            'client' => $client,
            'dataUrl' => route('monitoring.categorized', $client->username),
            'queryParams' => [],
        ]);
    }

    public function uncategorized(Client $client, Request $request)
    {
        if ($request->wantsJson()) {
            $response = $this->snippets->belongsToClient($client)
                ->endedAt(new DateTimeImmutable('-1 day'))
                ->sort($request->input('order'))
                ->take($request->input('length'))
                ->skip($request->input('start'))
                ->get();

            return $this->makeSearchResultResponse($client, $request, $response);
        }

        return view('monitoring.index', [
            'title' => $client->name . ' Monitoring, Uncategorized',
            'disableSourceFilter' => true,
            'client' => $client,
            'dataUrl' => route('monitoring.uncategorized', $client->username),
            'queryParams' => [],
        ]);
    }

    public function search(Client $client, Request $request)
    {
        if ($request->wantsJson()) {
            $daterange = new DateRangeHelper(
                $request->input('from', 'now'),
                $request->input('to', 'now')
            );

            $response = $this->snippets->belongsToClient($client)
                ->categorized()
                ->match($request->input('q'))
                ->sourceIn($request->input('sources', []))
                ->categoryIn($request->input('categories', []))
                ->startedAt($daterange->getStartedAt())
                ->endedAt($daterange->getEndedAt())
                ->sort($request->input('order'))
                ->take($request->input('length'))
                ->skip($request->input('start'))
                ->get();

            return $this->makeSearchResultResponse($client, $request, $response);
        }

        return view('monitoring.index', [
            'title' => $client->name . ' Monitoring, Categorized',
            'disableSourceFilter' => true,
            'client' => $client,
            'dataUrl' => route('monitoring.search', $client->username),
            'queryParams' => array_filter($request->only(['from', 'to', 'q', 'sources', 'categories'])),
        ]);
    }

    private function makeSearchResultResponse(Client $client, Request $request, array $data)
    {
        return response()->json([
            'draw' => $request->input('draw'),
            'recordsTotal' => $data['total'],
            'recordsFiltered' => $data['total'],
            'data' => array_map(function ($snippet) use ($client) {
                return array_merge($snippet, [
                    'timestamp' => (new DateTimeImmutable($snippet['timestamp']))->format('Y-m-d H:i:s'),
                    'links' => [
                        '_self' => route('snippets.update', [
                            'client' => $client->username,
                            'snippet' => $snippet['id']
                        ]),
                    ]
                ]);
            }, $data['snippets'])
        ]);
    }
}
