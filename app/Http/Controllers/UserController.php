<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            list($orderBy, $orderDir) = explode(',', $request->input('order', 'name,asc'));

            $users = User::orderBy($orderBy, $orderDir)
                ->skip($request->input('start', 0))
                ->take($request->input('length', 10))
                ->get();

            $count = User::count();

            return response()->json([
                'draw' => $request->input('draw'),
                'recordsTotal' => $count,
                'recordsFiltered' => $count,
                'data' => $users->map(function ($user) {
                    return [
                        'id' => $user->id,
                        'name' => $user->name,
                        'username' => $user->username,
                        'created_at' => $user->created_at->format('Y-m-d H:i:s'),
                        'links' => [
                            '_self' => route('users.self', $user->username),
                        ]
                    ];
                }),
            ]);
        }

        return view('users.index', ['page' => 'Users']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:users|unique:clients',
            'password' => 'required|confirmed',
        ]);

        User::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
        ]);

        return response()->json(['message' => 'Successfully add user']);
    }

    public function update(User $user, Request $request)
    {
        $this->validate($request, [
            'name' => 'required_without:password',
            'username' => [
                'required_without:password',
                Rule::unique('users')->ignore($user->id),
                'unique:clients'
            ],
            'password' => 'required_without_all:name,username|confirmed'
        ], [
            'required_without' => 'The :attribute fields is required.',
            'required_without_all' => 'The :attribute fields is required.',
        ]);

        $user->update(array_filter([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'password' => $request->has('password') ? bcrypt($request->input('password')) : null,
        ]));

        return response()->json(['message' => 'Successfully update user']);
    }

    public function remove(User $user, Request $request)
    {
        $user->delete();

        return response()->json(['message' => 'Successfully delete user']);
    }
}
