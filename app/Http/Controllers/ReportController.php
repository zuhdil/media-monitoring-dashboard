<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DateTimeImmutable;
use App\DateRangeHelper;
use App\Client;
use App\SnippetsDAO;

class ReportController extends Controller
{
    private $snippets;
    private $categories;
    private $sources;

    public function __construct(SnippetsDAO $snippets)
    {
        $this->snippets = $snippets;
        $this->categories = config('scraper.categories');

        $this->sources = [];
        foreach (config('scraper.sources') as $source) {
            $this->sources[$source['value']] = $source['label'];
        }
    }

    public function daily(Client $client, Request $request)
    {
        $daterange = new DateRangeHelper(
            '-1 day',
            $request->input('date', '-1 day'),
            'midnight'
        );

        $response = $this->snippets->summarizeCategoryForClient($client, $daterange->getStartedAt(), $daterange->getEndedAt());

        $summary = $response['by_category'];
        $summaryTotal = $response['count'];
        $summaryByCategory = [];
        $summaryBySource = [];
        foreach ($this->categories as $name => $info) {
            $summaryByCategory[$name] = [
                'label' => $info['label'],
                'count' => isset($summary[$name]) ? $summary[$name]['count'] : 0,
            ];
            if (isset($summary[$name])) {
                foreach ($summary[$name]['by_source'] as $source => $sourceInfo) {
                    if (! isset($summaryBySource[$source])) {
                        $summaryBySource[$source] = ['label' => $this->sources[$source], 'count' => 0];
                    }
                    $summaryBySource[$source]['count'] += (int) $sourceInfo['count'];
                }
            }
        }

        $samples = $this->getDailySnippetsGroupBySource($client, $daterange);

        return view('reports.daily', [
            'title' => $client->name . ' reports',
            'client' => $client,
            'date' => $daterange->getStartedAt(),
            'summaryByCategory' => $summaryByCategory,
            'summaryBySource' => $summaryBySource,
            'summaryTotal' => $summaryTotal,
            'samples' => $samples,
            'categories' => $this->categories,
        ]);
    }

    private function getDailySnippetsGroupBySource(Client $client, DateRangeHelper $daterange, $size = 10)
    {
        $samples = [];
        foreach ($this->getSourcesSortedByLabel() as $label => $source) {
            $sample = $this->snippets->belongsToClient($client)
                ->categorized()
                ->source($source)
                ->startedAt($daterange->getStartedAt())
                ->endedAt($daterange->getEndedAt())
                ->take($size)
                ->get();

            $samples[$label] = $sample['snippets'];
        }

        return $samples;
    }

    private function getSourcesSortedByLabel()
    {
        $sources = [];
        foreach (config('scraper.sources') as $key => $source) {
            $sources[$source['label']] = $key;
        }

        ksort($sources);

        return $sources;
    }

    public function monthly(Client $client, Request $request)
    {
        $date = new DateTimeImmutable($request->input('month', 'first day of previous month'));
        $maxDate = new DateTimeImmutable('midnight first day of this month');

        if ($date >= $maxDate) abort(404);

        $daterange = new DateRangeHelper(
            $date->modify('first day of this month'),
            $date->modify('last day of this month'),
            $maxDate,
            'month'
        );

        return view('reports.monthly', [
            'client' => $client,
            'startDate' => $daterange->getStartedAt(),
            'endDate' => $daterange->getEndedAt()->modify('-1 second'),
            'byDate' => $this->getDateHistogram($client, $daterange),
            'byHour' => $this->getDateHistogram($client, $daterange, 'hour'),
            'byCategory' => $this->getCategorySummary($client, $daterange),
        ]);
    }

    private function getCategorySummary(Client $client, DateRangeHelper $daterange)
    {
        $data = $this->snippets->summarizeCategoryForClient($client, $daterange->getStartedAt(), $daterange->getEndedAt());

        $result = [
            'count' => $data['count'],
            'by_category' => [],
        ];

        foreach ($data['by_category'] as $categoryId => $category) {
            if (isset($this->categories[$categoryId])) {
                $result['by_category'][$categoryId] = [
                    'label' => $this->categories[$categoryId]['label'],
                    'count' => $category['count'],
                    'by_source' => [],
                ];

                foreach ($category['by_source'] as $sourceId => $source) {
                    if (isset($this->sources[$sourceId])) {
                        $result['by_category'][$categoryId]['by_source'][$sourceId] = [
                            'label' => $this->sources[$sourceId],
                            'count' => $source['count'],
                        ];
                    }
                }
            }
        }

        return $result;
    }

    private function getDateHistogram(Client $client, DateRangeHelper $daterange, $interval = 'day')
    {
        $byDateData = $this->snippets->dateHistogramForClient($client, $daterange->getStartedAt(), $daterange->getEndedAt(), $interval);

        $byDate = ['all' => $this->formatDateHistogram($byDateData)];

        $byDate['by_source'] = [];
        foreach ($byDateData['by_source'] as $sourceId => $source) {
            if (isset($this->sources[$sourceId])) {
                $byDate['by_source'][$sourceId] = [
                    'label' => $this->sources[$sourceId],
                    'data' => $this->formatDateHistogram($source),
                ];
            }
        }

        return $byDate;
    }

    private function formatDateHistogram(array $data)
    {
        $categories = $this->categories;

        return [
            'count' => $data['count'],
            'columns' => array_merge(
                [['type' => 'date', 'label' => 'Date']],
                array_values(array_map(function ($c) {
                    return ['type' => 'number', 'label' => $c['label']];
                }, $categories)),
                [['type' => 'number', 'label' => 'Total']]
            ),
            'data' => array_map(
                function ($it, $date) use ($categories) {
                    return array_merge(
                        [new DateTimeImmutable($date)],
                        array_map(
                            function ($cat) use ($it) {
                                return isset($it['by_category'][$cat]) ? $it['by_category'][$cat]['count'] : 0;
                            },
                            array_keys($categories)
                        ),
                        [$it['count']]
                    );
                },
                $data['by_timestamp'],
                array_keys($data['by_timestamp'])
            ),
        ];
    }
}
