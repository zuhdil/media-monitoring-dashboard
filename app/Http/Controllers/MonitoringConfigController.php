<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\MonitoringConfig;

class MonitoringConfigController extends Controller
{
    public function index(Client $client, Request $request)
    {
        if ($request->wantsJson()) {
            list($orderBy, $orderDir) = explode(',', $request->input('order', 'started_at,desc'));

            $configs = $client->monitoringConfigs()
                ->orderBy($orderBy, $orderDir)
                ->skip($request->input('start', 0))
                ->take($request->input('length', 10))
                ->get();
            $count = $client->monitoringConfigs()->count();

            return response()->json([
                'draw' => $request->input('draw'),
                'recordsTotal' => $count,
                'recordsFiltered' => $count,
                'data' => $configs->map(function ($config) use ($client) {
                    return [
                        'id' => $config->id,
                        'keyword' => $config->keyword,
                        'started_at' => $config->started_at->format('Y-m-d H:i:s'),
                        'created_at' => $config->created_at->format('Y-m-d H:i:s'),
                        'links' => [
                            '_self' => route('clients.config.delete', [$client->username, $config->id])
                        ]
                    ];
                }),
            ]);
        }

        return view('monitoring.config', ['client' => $client, 'page' => 'Clients']);
    }

    public function store(Client $client, Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required',
            'started_at' => 'required|date',
        ]);

        $client->monitoringConfigs()->create([
            'keyword' => $request->input('keyword'),
            'started_at' => $request->input('started_at'),
        ]);

        return redirect()
            ->route('clients.config', $client->username)
            ->with('status', 'berhasil menambah konfigurasi monitoring');
    }

    public function remove(Client $client, MonitoringConfig $config)
    {
        $config->delete();

        return response()->json(['message' => 'Successfully delete config']);
    }
}
