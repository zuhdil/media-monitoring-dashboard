<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Elasticsearch\Common\Exceptions\Missing404Exception;
use Exception;
use App\Client;
use App\SnippetsDAO;

class SnippetController extends Controller
{
    private $snippets;

    public function __construct(SnippetsDAO $snippets)
    {
        $this->snippets = $snippets;
    }

    public function update(Client $client, $snippet, Request $request)
    {
        try {
            $this->snippets->update($snippet, ['category' => $request->input('category')]);
        }
        catch (Missing404Exception $e) {
            abort(404);
        }

        return response()->json(['message' => 'Successfully update content']);
    }

    public function bulkUpdate(Client $client, Request $request)
    {
        $category = $request->input('category');
        $snippets = $request->input('snippets');
        $params = [];

        foreach ($snippets as $snippet) {
            $params[$snippet] = ['category' => $category];
        }

        $this->snippets->bulkUpdate($params);

        return response()->json(['message' => 'Successfully update contents']);
    }
}
