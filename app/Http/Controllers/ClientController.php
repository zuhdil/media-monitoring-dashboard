<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use Illuminate\Validation\Rule;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        if ($request->wantsJson()) {
            list($orderBy, $orderDir) = explode(',', $request->input('order', 'name,asc'));

            $clients = Client::with('monitoringConfigs')
                ->orderBy($orderBy, $orderDir)
                ->skip($request->input('start', 0))
                ->take($request->input('length', 10))
                ->get();

            $count = Client::count();

            return response()->json([
                'draw' => $request->input('draw'),
                'recordsTotal' => $count,
                'recordsFiltered' => $count,
                'data' => $clients->map(function ($client) {
                    $keywords = $client->monitoringConfigs
                        ->sortByDesc('started_at')
                        ->pluck('keyword');

                    return [
                        'id' => $client->id,
                        'name' => $client->name,
                        'username' => $client->username,
                        'keywords' => $keywords,
                        'is_active' => $client->is_active,
                        'created_at' => $client->created_at->format('Y-m-d H:i:s'),
                        'links' => [
                            '_self' => route('clients.self', $client->username),
                            'monitoring' => route('monitoring', $client->username),
                            'config' => route('clients.config', $client->username),
                            'active' => route('clients.active', $client->username),
                        ],
                    ];
                }),
            ]);
        }

        return view('clients.index', ['page' => 'Clients']);
    }

    public function create()
    {
        return view('clients.create', ['page' => 'Clients']);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required|unique:clients|unique:users',
            'password' => 'required|confirmed',
        ]);

        Client::create([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'password' => bcrypt($request->input('password')),
        ]);

        return redirect()->route('clients')->with('status', 'Successfully add client');
    }

    public function update(Client $client, Request $request)
    {
        $this->validate($request, [
            'name' => 'required_without:password',
            'username' => [
                'required_without:password',
                'unique:users',
                Rule::unique('clients')->ignore($client->id)
            ],
            'password' => 'required_without_all:name,username|confirmed',
        ], [
            'required_without' => 'The :attribute fields is required.',
            'required_without_all' => 'The :attribute fields is required.',
        ]);

        $client->update(array_filter([
            'name' => $request->input('name'),
            'username' => $request->input('username'),
            'password' => $request->has('password') ? bcrypt($request->input('password')) : null,
        ]));

        return response()->json(['message' => 'Successfully update client']);
    }

    public function deactivate(Client $client)
    {
        $client->update(['is_active' => false]);

        return response()->json(['message' => 'Successfully deactivate client']);
    }

    public function activate(Client $client)
    {
        $client->update(['is_active' => true]);

        return response()->json(['message' => 'Successfully activate client']);
    }

    public function remove(Client $client)
    {
        $client->delete();

        return response()->json(['message' => 'Successfully delete client']);
    }
}
