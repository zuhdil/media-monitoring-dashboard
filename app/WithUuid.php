<?php
namespace App;

use Ramsey\Uuid\Uuid;

trait WithUuid
{
    protected static function boot()
    {
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string)Uuid::uuid4();
        });
    }
}
