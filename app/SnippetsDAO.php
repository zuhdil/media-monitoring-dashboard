<?php
namespace App;

use Elasticsearch\Client as Elasticsearch;
use Ramsey\Uuid\Uuid;
use DateTimeInterface;

class SnippetsDAO
{
    private $client;
    private $sources;
    private $categories;
    private $index;
    private $type;

    public function __construct(Elasticsearch $client, array $sources, array $categories, $index, $type = 'snippets')
    {
        $this->client = $client;
        $this->sources = $sources;
        $this->categories = $categories;
        $this->index = $index;
        $this->type = $type;
    }

    public function bulkIndex(array $snippets, $refresh = false)
    {
        $params = ['body' => []];

        foreach ($snippets as $snippet) {
            $params['body'][] = ['index' => [
                '_index' => $this->index,
                '_type' => $this->type,
                '_id' => $snippet['id'],
            ]];
            $params['body'][] = $snippet;
        }

        if (true === $refresh) {
            $params['refresh'] = true;
        }

        $this->client->bulk($params);
    }

    public function update($id, $doc)
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id,
            'body' => ['doc' => $doc]
        ];

        $this->client->update($params);
    }

    public function bulkUpdate(array $snippets, $refresh = false)
    {
        $params = ['body' => []];

        foreach ($snippets as $id => $doc) {
            $params['body'][] = ['update' => [
                '_index' => $this->index,
                '_type' => $this->type,
                '_id' => $id,
            ]];
            $params['body'][] = ['doc' => $doc];
        }

        if (true === $refresh) {
            $params['refresh'] = true;
        }

        $this->client->bulk($params);
    }

    public function get($id)
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'id' => $id,
        ];

        $response = $this->client->get($params);

        return $response['_source'];
    }

    public function belongsToClient(Client $client)
    {
        return new ClientSnippetsQuery($this, $client, $this->sources, $this->categories);
    }

    public function summarizeCategoryForClient(Client $client, DateTimeInterface $start, DateTimeInterface $end)
    {
        $body = [
            "query" => [
                "bool" => [
                    "filter" => [
                        "bool" => [
                            "must" => [
                                [
                                    "term" => ["owner" => $client->id],
                                ],
                                [
                                    "exists" => ["field"  => "category"],
                                ],
                                [
                                    "range" => [
                                        "timestamp" => [
                                            "gte" => $start->format(DATE_ISO8601),
                                            "lt" => $end->format(DATE_ISO8601),
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            "size" => 0,
            "aggregations" => [
                "by_category" => [
                    "terms" => [
                        "field" => "category",
                    ],
                    "aggregations" => [
                        "by_source" => [
                            "terms" => [
                                "field" => "source",
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $body,
        ];

        $response = $this->client->search($params);

        return [
            'count' => $response['hits']['total'],
            'by_category' => array_reduce(
                $response['aggregations']['by_category']['buckets'],
                function ($carry, $item) {
                    $carry[$item['key']] = [
                        'count' => $item['doc_count'],
                        'by_source' => array_reduce(
                            $item['by_source']['buckets'],
                            function ($carry, $item) {
                                $carry[$item['key']] = ['count' => $item['doc_count']];
                                return $carry;
                            },
                            []
                        ),
                    ];

                    return $carry;
                },
                []
            ),
        ];
    }

    public function dateHistogramForClient(Client $client, DateTimeInterface $start, DateTimeInterface $end, $interval = 'day')
    {
        $body = [
            "query" => [
                "bool" => [
                    "filter" => [
                        "bool" => [
                            "must" => [
                                [
                                    "term" => ["owner" => $client->id],
                                ],
                                [
                                    "exists" => ["field"  => "category"],
                                ],
                                [
                                    "range" => [
                                        "timestamp" => [
                                            "gte" => $start->format(DATE_ISO8601),
                                            "lt" => $end->format(DATE_ISO8601),
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            "size" => 0,
            "aggregations" => [
                "by_timestamp" => [
                    "date_histogram" => [
                        "field" => "timestamp",
                        "interval" => $interval,
                        "time_zone" => $start->format('P'),
                    ],
                    "aggregations" => [
                        "by_category" => [
                            "terms" => [
                                "field" => "category",
                            ]
                        ],
                    ],
                ],
                "by_source" => [
                    "terms" => [
                        "field" => "source",
                    ],
                    "aggregations" => [
                        "by_timestamp" => [
                            "date_histogram" => [
                                "field" => "timestamp",
                                "interval" => $interval,
                                "time_zone" => $start->format('P'),
                            ],
                            "aggregations" => [
                                "by_category" => [
                                    "terms" => [
                                        "field" => "category",
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];

        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $body,
        ];

        $response = $this->client->search($params);

        return [
            'count' => $response['hits']['total'],
            'by_timestamp' => array_reduce(
                $response['aggregations']['by_timestamp']['buckets'],
                function ($carry, $item) {
                    $carry[$item['key_as_string']] = [
                        'count' => $item['doc_count'],
                        'by_category' => array_reduce(
                            $item['by_category']['buckets'],
                            function ($carry, $item) {
                                $carry[$item['key']] = ['count' => $item['doc_count']];
                                return $carry;
                            },
                            []
                        ),
                    ];

                    return $carry;
                },
                []
            ),
            'by_source' => array_reduce(
                $response['aggregations']['by_source']['buckets'],
                function ($carry, $item) {
                    $carry[$item['key']] = [
                        'count' => $item['doc_count'],
                        'by_timestamp' => array_reduce(
                            $item['by_timestamp']['buckets'],
                            function ($carry, $item) {
                                $carry[$item['key_as_string']] = [
                                    'count' => $item['doc_count'],
                                    'by_category' => array_reduce(
                                        $item['by_category']['buckets'],
                                        function ($carry, $item) {
                                            $carry[$item['key']] = ['count' => $item['doc_count']];
                                            return $carry;
                                        },
                                        []
                                    )
                                ];

                                return $carry;
                            },
                            []
                        )
                    ];

                    return $carry;
                },
                []
            ),
        ];
    }

    public function search(array $params)
    {
        $params = [
            'index' => $this->index,
            'type' => $this->type,
            'body' => $params,
        ];

        $response = $this->client->search($params);
        $hits = $response['hits'];

        return [
            'total' => $hits['total'],
            'snippets' => array_map(function ($hit) {
                return $hit['_source'];
            }, $hits['hits']),
        ];
    }

    public function install()
    {
        $this->uninstall();

        $params = [
            'index' => $this->index,
            'body' => [
                'mappings' => [
                    $this->type => [
                        '_source' => ['enabled' => true],
                        'properties' => [
                            'id' => ['type' => 'keyword'],
                            'owner' => ['type' => 'keyword'],
                            'source' => ['type' => 'keyword'],
                            'title' => [
                                'type' => 'text',
                                'fields' => [
                                    'ind' => [
                                        'type' => 'text',
                                        'analyzer' => 'indonesian',
                                    ],
                                    'raw' => [
                                        'type' => 'keyword'
                                    ]
                                ]
                            ],
                            'snippet' => [
                                'type' => 'text',
                                'fields' => [
                                    'ind' => [
                                        'type' => 'text',
                                        'analyzer' => 'indonesian',
                                    ],
                                    'raw' => [
                                        'type' => 'keyword'
                                    ]
                                ]
                            ],
                            'url' => [
                                'type' => 'text',
                                'fields' => [
                                    'raw' => [
                                        'type' => 'keyword'
                                    ]
                                ]
                            ],
                            'category' => ['type' => 'keyword'],
                            'timestamp' => ['type' => 'date'],
                        ]
                    ]
                ]
            ]
        ];

        $this->client->indices()->create($params);
    }

    public function uninstall()
    {
        if ($this->client->indices()->exists(['index' => $this->index])) {
            $this->client->indices()->delete(['index' => $this->index]);
        }
    }
}
