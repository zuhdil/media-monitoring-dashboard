<?php

use Illuminate\Database\Migrations\Migration;
use App\SnippetsDAO;

class CreateMonitoringSnippetsElasticsearchIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dao = app(SnippetsDAO::class);
        $dao->install();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $dao = app(SnippetsDAO::class);
        $dao->uninstall();
    }
}
