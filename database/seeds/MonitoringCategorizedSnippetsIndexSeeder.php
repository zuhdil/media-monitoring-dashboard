<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\SnippetsDAO;
use App\Client;

class MonitoringCategorizedSnippetsIndexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dao = app(SnippetsDAO::class);
        $faker = app(Faker::class);
        $self = $this;

        $this->sources = array_values(array_map(function ($source) {
            return $source['value'];
        }, config('scraper.sources')));

        $this->categories = array_keys(config('scraper.categories'));

        Client::all()->each(function (Client $client) use ($dao, $faker, $self) {
            $dao->bulkIndex($self->generateSnippets($faker, $client));
        });
    }

    private function generateSnippets(Faker $faker, Client $owner)
    {
        $snippets = [];

        for ($i = 0, $length = random_int(500, 1000); $i < $length; $i++) {
            $snippets[] = [
                'id' => $faker->uuid,
                'owner' => $owner->id,
                'source' => $faker->randomElement($this->sources),
                'category' => $faker->randomElement($this->categories),
                'title' => $faker->sentence(),
                'snippet' => $faker->paragraph(),
                'url' => $faker->url,
                'timestamp' => $faker->dateTimeBetween('-3 month', '-2 day')->format(DATE_ISO8601),
            ];
        }

        return $snippets;
    }
}
