<?php

use Illuminate\Database\Seeder;
use App\Client;
use App\MonitoringConfig;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = factory(Client::class)->create(['username' => 'kelana']);
        $client->monitoringConfigs()->saveMany(
            factory(MonitoringConfig::class, random_int(2, 5))->make()
        );

        $length = random_int(10, 25);

        for ($i = 0; $i < $length; $i++) {
            $client = factory(Client::class)->create();
            $client->monitoringConfigs()->saveMany(
                factory(MonitoringConfig::class, random_int(2, 5))->make()
            );
        }
    }
}
