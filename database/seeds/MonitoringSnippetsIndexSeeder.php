<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use App\SnippetsDAO;
use App\Client;

class MonitoringSnippetsIndexSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dao = app(SnippetsDAO::class);
        $faker = app(Faker::class);
        $self = $this;

        Client::all()->each(function ($client) use ($dao, $faker, $self) {
            $dao->bulkIndex($self->generateSnippets($faker, $client->id));
        });
    }

    private function generateSnippets(Faker $faker, $owner)
    {
        $snippets = [];

        for ($i = 0, $length = random_int(20, 100); $i < $length; $i++) {
            $snippets[] = [
                'id' => $faker->uuid,
                'owner' => $owner,
                'source' => $faker->randomElement([
                    'www.google.com',
                    'www.bing.com',
                    'twitter.com',
                ]),
                'title' => $faker->sentence(),
                'snippet' => $faker->paragraph(),
                'url' => $faker->url,
                'timestamp' => $faker->dateTimeBetween('-2 day', 'now')->format(DATE_ISO8601),
            ];
        }

        return $snippets;
    }
}
