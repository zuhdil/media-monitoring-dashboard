<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'created_at' => $faker->dateTimeThisMonth('-1 week'),
    ];
});

$factory->define(App\Client::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'username' => $faker->userName,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'created_at' => $faker->dateTimeThisMonth('-1 week'),
    ];
});

$factory->define(App\MonitoringConfig::class, function (Faker\Generator $faker) {
    return [
        'keyword' => $faker->sentence(3),
        'started_at' => $faker->dateTimeBetween('-1 week', '-2 day'),
        'created_at' => $faker->dateTimeThisMonth('-1 week'),
    ];
});
