# Brand Monitoring Dashboard


## Minimum requirement

- PHP 5.6
- Java 1.8.0_73
- Elasticsearch 5.0.1
- Mysql 5.6


## Development notes

### PHP

Install the project dependencies using composer.

  $ composer install

Setup environment variable by copying the `.env.example` file to `.env` file.

  $ cp .env.example .env

Generate application key.

  $ php artisan key:generate

Create the database and edit the environment variables in `.env` file to match your system configuration.

Run database migration and populate testing data. Make sure elasticsearch is running before executing migration command.

  $ php artisan migrate --seed

For production server, only seed the users table to populate the required user.

  $ php artisan migrate
  $ php artisan db:seed --class=UsersTableSeeder


### Testing

To run the phpunit tests, first make sure the `DB_TEST_HOST`, `DB_TEST_PORT`, `DB_TEST_DATABASE`, `DB_TEST_USERNAME`, and `DB_TEST_PASSWORD` is set properly.

Run database migration on testing database.

  $ php artisan migrate --database=mysql_testing

Run the test using the following command.

  $ php vendor/bin/phpunit


### Web assets

Asset development (javascript, css, etc.) is using Laravel Elixir which utilize gulp, webpack, and sass. Therefore, Node.js and npm is required.

Install the project dependencies using npm

  $ npm install

For development, run the development build pipeline to auto compile on file changes.

  $ npm run dev

For production, run the production build pipeline to compile assets.

  $ npm run prod

