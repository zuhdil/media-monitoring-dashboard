<?php

return [
    'elasticsearch_index' => env('SCRAPER_ELASTICSEARCH_INDEX', 'brand-monitoring'),

    'sources' => [
        'twitter' => [
            'label' => 'Twitter',
            'value' => 'twitter.com',
        ],
        'google' => [
            'label' => 'Google',
            'value' => 'www.google.com',
        ],
        'bing' => [
            'label' => 'Bing',
            'value' => 'www.bing.com',
        ],
        'facebook' => [
            'label' => 'Facebook',
            'value' => 'www.facebook.com',
        ],
        'kaskus' => [
            'label' => 'Kaskus',
            'value' => 'www.kaskus.co.id',
        ],
        'detik' => [
            'label' => 'Detik',
            'value' => 'www.detik.com',
        ],
        'play' => [
            'label' => 'Google Play',
            'value' => 'play.google.com',
        ],
    ],

    'categories' => [
        'safe-content' => [
            'abbr' => 'O',
            'label' => 'Safe content'
        ],
        'phising' => [
            'abbr' => 'P',
            'label' => 'Phising'
        ],
        'spam' => [
            'abbr' => 'S',
            'label' => 'Spam'
        ],
        'contain-virus' => [
            'abbr' => 'Cv',
            'label' => 'Contain virus'
        ],
        'information-leaks' => [
            'abbr' => 'IL',
            'label' => 'Information leaks'
        ],
        'fake-domain' => [
            'abbr' => 'FD',
            'label' => 'Fake domain'
        ],
        'negative-content' => [
            'abbr' => 'NC',
            'label' => 'Negative content'
        ],
    ],
];
