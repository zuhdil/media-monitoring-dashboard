<?php

return [
    'hosts' => explode(',', env('ELASTICSEARCH_HOSTS', 'localhost:9200')),
    'logPath' => storage_path('logs/elasticsearch.log'),
];
