const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app.scss');

    mix.styles([
        'public/css/app.css',
        'node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
        'node_modules/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
        'node_modules/datatables.net-bs/css/dataTables.bootstrap.css',
        'resources/assets/css/theme.css',
        'resources/assets/css/overrides.css',
    ], 'public/css/styles.css', './');

    mix.copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts');

    mix.webpack('app.js');
});
